<?php

namespace DataBundle\Entity;

/**
 * Media
 */
class Media
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $urlpath;

    /**
     * @var string
     */
    private $status;

    /**
     * @var \DateTime
     */
    private $uploadeddate;

    /**
     * @var \DataBundle\Entity\Teachers
     */
    private $teacherid;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $studentsid;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->studentsid = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Media
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Media
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set urlpath
     *
     * @param string $urlpath
     *
     * @return Media
     */
    public function setUrlpath($urlpath)
    {
        $this->urlpath = $urlpath;

        return $this;
    }

    /**
     * Get urlpath
     *
     * @return string
     */
    public function getUrlpath()
    {
        return $this->urlpath;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Media
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set uploadeddate
     *
     * @param \DateTime $uploadeddate
     *
     * @return Media
     */
    public function setUploadeddate($uploadeddate)
    {
        $this->uploadeddate = $uploadeddate;

        return $this;
    }

    /**
     * Get uploadeddate
     *
     * @return \DateTime
     */
    public function getUploadeddate()
    {
        return $this->uploadeddate;
    }

    /**
     * Set teacherid
     *
     * @param \DataBundle\Entity\Teachers $teacherid
     *
     * @return Media
     */
    public function setTeacherid(\DataBundle\Entity\Teachers $teacherid = null)
    {
        $this->teacherid = $teacherid;

        return $this;
    }

    /**
     * Get teacherid
     *
     * @return \DataBundle\Entity\Teachers
     */
    public function getTeacherid()
    {
        return $this->teacherid;
    }

    /**
     * Add studentsid
     *
     * @param \DataBundle\Entity\Students $studentsid
     *
     * @return Media
     */
    public function addStudentsid(\DataBundle\Entity\Students $studentsid)
    {
        $this->studentsid[] = $studentsid;

        return $this;
    }

    /**
     * Remove studentsid
     *
     * @param \DataBundle\Entity\Students $studentsid
     */
    public function removeStudentsid(\DataBundle\Entity\Students $studentsid)
    {
        $this->studentsid->removeElement($studentsid);
    }

    /**
     * Get studentsid
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStudentsid()
    {
        return $this->studentsid;
    }
}
