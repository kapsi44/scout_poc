<?php

namespace DataBundle\Entity;

use Symfony\Component\Security\Core\User\UserInterface;

class UserDecorator implements UserInterface, \Serializable 
{
    protected $teachers;

    const method = 'AES-256-CBC';
    const secret_key = 'fe67d68ee1e09b47acd8810b880d537034c10c15344433a992b9c79002666844';
    const secret_iv = 'fdd3345455fffgffffhkkyoife67d68ee1e09b47acd8810b880d537034c10c15344433a992b9c79002666844';
    
    public function __construct(Teachers $teachers)
    {
        $this->teachers = $teachers;
    }
    
    public function getUserName()
    {
        return $this->teachers->getEmailid();
    }

    public function getUserId()
    {
        return $this->teachers->getId();
    }
    
    public function getPassword()
    {
        return $this->teachers->getPassword();
    }
    
    public function getSalt()
    {
        return null;
    }
    
    public function getRoles()
    {
        return array('ROLE_USER');
    }
    
    public function eraseCredentials()
    {
    }
    
    public function serialize()
    {
        return null;
    }

    public function unserialize($serialized)
    {
    }

    function encrypt_decrypt($action, $string) 
    {
        $output = false;
        $key = hash('sha256', self::secret_key);
        $iv = substr(hash('sha256', self::secret_iv), 0, 16);

        if ($action == 'encrypt') {
            $output = openssl_encrypt($string, self::method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if($action == 'decrypt') {
            $output = openssl_decrypt(base64_decode($string), self::method, $key, 0, $iv);
        }
 
        return $output;
    }

    function base64_encodeDecode($string, $action='decode') {
        return $action == "encode" ? base64_encode($string) : base64_decode($string);
    }
}