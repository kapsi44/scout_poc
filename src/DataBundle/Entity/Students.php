<?php

namespace DataBundle\Entity;

use DataBundle\Entity\UserDecorator;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @UniqueEntity(fields={"emailid"}, message="It looks like this student already have an account!")
 */
class Students
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $emailid;

    /**
     * @var string
     */
    private $dob;

    /**
     * @var string
     */
    private $grade;

    /**
     * @var string
     */
    private $status;

    /**
     * @var \DateTime
     */
    private $createddate;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $mediaid;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $teacherid;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->mediaid = new \Doctrine\Common\Collections\ArrayCollection();
        $this->teacherid = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Students
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set emailid
     *
     * @param string $emailid
     *
     * @return Students
     */
    public function setEmailid($emailid)
    {
        $this->emailid = $emailid;

        return $this;
    }

    /**
     * Get emailid
     *
     * @return string
     */
    public function getEmailid()
    {
        return $this->emailid;
    }

    /**
     * Set dob
     *
     * @param string $dob
     *
     * @return Students
     */
    public function setDob($dob)
    {
        $this->dob = $dob;

        return $this;
    }

    /**
     * Get dob
     *
     * @return string
     */
    public function getDob()
    {
        return $this->dob;
    }

    /**
     * Set grade
     *
     * @param string $grade
     *
     * @return Students
     */
    public function setGrade($grade)
    {
        $this->grade = $grade;

        return $this;
    }

    /**
     * Get grade
     *
     * @return string
     */
    public function getGrade()
    {
        return $this->grade;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Students
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createddate
     *
     * @param \DateTime $createddate
     *
     * @return Students
     */
    public function setCreateddate($createddate)
    {
        $this->createddate = $createddate;

        return $this;
    }

    /**
     * Get createddate
     *
     * @return \DateTime
     */
    public function getCreateddate()
    {
        return $this->createddate;
    }

    /**
     * Add mediaid
     *
     * @param \DataBundle\Entity\Media $mediaid
     *
     * @return Students
     */
    public function addMediaid(\DataBundle\Entity\Media $mediaid)
    {
        $this->mediaid[] = $mediaid;

        return $this;
    }

    /**
     * Remove mediaid
     *
     * @param \DataBundle\Entity\Media $mediaid
     */
    public function removeMediaid(\DataBundle\Entity\Media $mediaid)
    {
        $this->mediaid->removeElement($mediaid);
    }

    /**
     * Get mediaid
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMediaid()
    {
        return $this->mediaid;
    }

    /**
     * Add teacherid
     *
     * @param \DataBundle\Entity\Teachers $teacherid
     *
     * @return Students
     */
    public function addTeacherid(\DataBundle\Entity\Teachers $teacherid)
    {
        $this->teacherid[] = $teacherid;

        return $this;
    }

    /**
     * Remove teacherid
     *
     * @param \DataBundle\Entity\Teachers $teacherid
     */
    public function removeTeacherid(\DataBundle\Entity\Teachers $teacherid)
    {
        $this->teacherid->removeElement($teacherid);
    }

    /**
     * Get teacherid
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTeacherid()
    {
        return $this->teacherid;
    }
}
