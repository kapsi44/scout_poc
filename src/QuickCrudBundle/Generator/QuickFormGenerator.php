<?php

namespace QuickCrudBundle\Generator;

use Petkopara\CrudGeneratorBundle\Generator\PetkoparaFormGenerator;
use Petkopara\CrudGeneratorBundle\Generator\Guesser\MetadataGuesser;
use Symfony\Component\HttpKernel\Bundle\BundleInterface;
use Doctrine\Common\Util\Inflector;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use RuntimeException;

class QuickFormGenerator extends PetkoparaFormGenerator 
{
    private $className;
    private $classPath;
    private $metadataGuesser;
    
    public function __construct(MetadataGuesser $guesser)
    {
        $this->metadataGuesser = $guesser;
    }
    
    public function getClassName()
    {
        return $this->className;
    }

    public function getClassPath()
    {
        return $this->classPath;
    }
    
    public function generate(BundleInterface $bundle, $entities, ClassMetadataInfo $metadata, $forceOverwrite = false, $fileUploadField = null, $recursiveAssoc = null)
    {
        $entity = $this->getEntityClass($entities);
        $parts = explode('\\', $entity);
        $entityClass = array_pop($parts);

        $this->className = $entityClass . 'Type';
        $dirPath = $bundle->getPath() . '/Form';
        $this->classPath = $dirPath . '/' . str_replace('\\', '/', $entity) . 'Type.php';

        if (!$forceOverwrite && file_exists($this->classPath)) {
            throw new RuntimeException(sprintf('Unable to generate the %s form class as it already exists under the %s file', $this->className, $this->classPath));
        }

        $this->setIdentifier($metadata);
        
        $parts = explode('\\', $entity);
        array_pop($parts);
        
        if ($this->isManyToMany($metadata) && $this->isOwingSide($metadata)) {
            $this->generateManageForm($bundle, $entities, $metadata, $forceOverwrite, $fileUploadField);
            $this->generateManageSubscriber($bundle, $entities, $metadata);
            $this->generateDataTransformer($bundle, $entities, $metadata);
        }
        $fieldsWithTypes = $this->getFieldsType($metadata);

        $this->renderFile('form/FormType.php.twig', $this->classPath, array(
            'fields' => $this->getFieldsFromEntityMetadata($metadata),
            'fields_with_types' => $fieldsWithTypes,
            'fields_associated' => $this->getAssociatedFields($metadata),
            'fields_mapping' => $metadata->fieldMappings,
            'file_field_name' => $fileUploadField,
            'namespace' => $bundle->getNamespace(),
            'entity_bundle' => 'DataBundle',
            'entity_namespace' => implode('\\', $parts),
            'entity_class' => $entityClass,
            'bundle' => $bundle->getName(),
            'form_class' => $this->className,
            'form_type_name' => strtolower(str_replace('\\', '_', $bundle->getNamespace()) . ($parts ? '_' : '') . implode('_', $parts) . '_' . substr($this->className, 0, -4)),
            // Add 'setDefaultOptions' method with deprecated type hint, if the new 'configureOptions' isn't available.
            // Required as long as Symfony 2.6 is supported.
            'configure_options_available' => method_exists('Symfony\Component\Form\AbstractType', 'configureOptions'),
            'get_name_required' => !method_exists('Symfony\Component\Form\AbstractType', 'getBlockPrefix'),
        ));
    }
    
    private function generateManageForm(BundleInterface $bundle, $entities, ClassMetadataInfo $metadata, $forceOverwrite = false, $fileUploadField = null)
    {
        $entity = $this->getEntityClass($entities);
        
        $fields = $this->getManyToManyFields($metadata);
        
        foreach ($fields as $field) {
            $targetEntity = explode('\\', $field['class']);
            $targetEntityClass = array_pop($targetEntity);
            $targetEntityId = $this->getTargetEntityId($metadata);
            $targetPrimarKey = Inflector::classify($targetEntityId['id']);
            $manageMethod = $entity . $targetEntityClass;
                
            $dirPath = $bundle->getPath() . '/Form';
            $classPath = $dirPath . '/' . $manageMethod . 'Type.php';
            
            if (!$forceOverwrite && file_exists($classPath)) {
                throw new RuntimeException(sprintf('Unable to generate the %s form class as it already exists under the %s file', $manageClassName, $classPath));
            }

            $this->setIdentifier($metadata);

            $parts = explode('\\', $entity);
            array_pop($parts);
            
            $this->renderFile('form/ManageFormType.php.twig', $classPath, array(
                'namespace' => $bundle->getNamespace(),
                'entity_bundle' => 'DataBundle',
                'file_field_name' => $fileUploadField,
                'entity_class' => $entity,
                'field_name' => $field['name'],
                'entity_namespace' => implode('\\', $parts),
                'manage_class_name' => $manageMethod,
                'bundle' => $bundle->getName(),
                'target_entity' => $targetEntityClass,
                'owing_entity_singular' => lcfirst($entity),
                'fields' => $field,
                'target_primary_key' => $targetPrimarKey,
            ));
        }       
        
    }
    
    public function generateManageSubscriber(BundleInterface $bundle, $entities, ClassMetadataInfo $metadata)
    {
        $entity = $this->getEntityClass($entities);
        
        $fields = $this->getManyToManyFields($metadata);
        
        foreach ($fields as $field) {
            $targetEntity = explode('\\', $field['class']);
            $targetEntityClass = array_pop($targetEntity);
            $targetEntityId = $this->getTargetEntityId($metadata);
            $targetPrimarKey = Inflector::classify($targetEntityId['id']);
            $manageMethod = $entity . $targetEntityClass;
            
            $dirPath = $bundle->getPath() . '/Form';
            $subscriberPath = $dirPath . '/EventListener/' . $manageMethod . 'Subscriber.php';
            $this->renderFile('form/eventListener/ManageSubscriber.php.twig', $subscriberPath, array(
                'namespace' => $bundle->getNamespace(),
                'entity_bundle' => 'DataBundle',
                'manage_method' => $manageMethod,
                'field_name' => $field['name'],
                'entity_class' => $entity,
                'entity_class_singular' => lcfirst($entity),
                'tareget_entity_class' => $targetEntityClass,
                'tareget_entity_class_singular' => lcfirst($targetEntityClass),
                'tareget_entity' => $field['class'],
                'target_primary_key' => $targetPrimarKey,
            ));
        }
    }
    
    public function generateDataTransformer(BundleInterface $bundle, $entities, ClassMetadataInfo $metadata)
    {
        $entity = $this->getEntityClass($entities);
        
        $fields = $this->getManyToManyFields($metadata);
        
        foreach ($fields as $field) {
            $targetEntity = explode('\\', $field['class']);
            $targetEntityClass = array_pop($targetEntity);
            $targetEntityId = $this->getTargetEntityId($metadata);
            $targetPrimarKey = Inflector::classify($targetEntityId['id']);
            $manageMethod = $entity . $targetEntityClass;
            
            $dirPath = $bundle->getPath() . '/Form';
            
            $dataTransformerPath = $dirPath . '/DataTransformer/' . $manageMethod . 'Transformer.php';
        
            $this->renderFile('form/dataTransformer/DataTransformer.php.twig', $dataTransformerPath, array(
                'namespace' => $bundle->getNamespace(),
                'entity_bundle' => 'DataBundle',
                'manage_method' => $manageMethod,
                'field_name' => $field['name'],
                'entity_class' => $entity,
                'entity_class_singular' => lcfirst($entity),
                'tareget_entity_class' => $targetEntityClass,
                'tareget_entity_class_singular' => lcfirst($targetEntityClass),
                'tareget_entity' => $field['class'],
                'target_primary_key' => $targetPrimarKey,
            ));
        }
    }

    private function getFieldsFromEntityMetadata(ClassMetadataInfo $metadata)
    {
        $fields = (array) $metadata->fieldNames;
        
        // Remove the primary key field if it's not managed manually
        if (!$metadata->isIdentifierNatural()) {
            $fields = array_diff($fields, $metadata->identifier);
        }

        return $fields;
    }
    
    private function getAssociatedFields(ClassMetadataInfo $metadata)
    {
        $fields = array();
        
        foreach ($metadata->associationMappings as $fieldName => $relation) {

            switch ($relation['type']) {
                case ClassMetadataInfo::ONE_TO_ONE:
                case ClassMetadataInfo::MANY_TO_ONE:
                case ClassMetadataInfo::MANY_TO_MANY:
                    if ($relation['isOwningSide']) {
                        $fields[$fieldName] = $this->getRelationFieldData($fieldName, $relation, $relation['type']);
                    }
                    break;
                case ClassMetadataInfo::ONE_TO_MANY:
                    if ($relation['mappedBy']) {
                        $fields[$fieldName] = $this->getIverseFieldData($fieldName, $relation, $relation['type']);
                    }
                    break;
            }
        }

        return $fields;
    }
    
    private function getManyToManyFields(ClassMetadataInfo $metadata)
    {
        $fields = array();
        
        foreach ($metadata->associationMappings as $fieldName => $relation) {
            if ($relation['type'] === ClassMetadataInfo::MANY_TO_MANY && $relation['isOwningSide']) {
                $fields[$fieldName] = $this->getRelationFieldData($fieldName, $relation, $relation['type']);
            }
        }
        
        return $fields;
    }


    private function getRelationFieldData($fieldName, $relation, $relationType)
    {
        $field = array();
        $field['name'] = $fieldName;
        $field['widget'] = 'EntityType::class';
        $field['class'] = $relation['targetEntity'];
        $field['choice_label'] = $this->metadataGuesser->guessChoiceLabelFromClass($relation['targetEntity']);
        $field['type'] = $relationType;
        
        return $field;
    }
    
    private function getIverseFieldData($fieldName, $relation, $relationType)
    {
        $field = array();
        
        $field['name'] = $fieldName;
        $field['widget'] = 'CollectionType::class';
        $targetEntity = explode("\\", $relation['targetEntity']);
        $field['entry_type'] = array_pop($targetEntity) . "Type";
        $field['type'] = $relationType;
        
        return $field;
    }

    private function getEntityClass($entity)
    {
        $entities = str_replace("\\", " ", $entity);
        $entityPath = explode(" ", $entities);
        
        return array_pop($entityPath);
    }
    
    private function isManyToMany($metadata)
    {
        foreach ($metadata->associationMappings as $fieldName => $relation) {
            if ($relation['type'] === ClassMetadataInfo::MANY_TO_MANY) {
                return true;
            } 
        }
    }
    
    private function isOwingSide($metadata)
    {
        foreach ($metadata->associationMappings as $fieldName => $relation) {
            if ($relation['type'] === ClassMetadataInfo::MANY_TO_MANY && $relation['isOwningSide']) {
                return true;
            }
        }
    }
    
    private function getTargetEntityName($metadata)
    {
        $fields = array();
        
        foreach ($metadata->associationMappings as $fieldName => $relation) {
            if ($relation['type'] === ClassMetadataInfo::MANY_TO_MANY) {
                $fields['field_name'] = $fieldName;
                $fields['target_entity_class'] = ucfirst($fieldName);
                $fields['target_entity'] = $relation['targetEntity'];
            }
        }
        
        return $fields;
    }
    
    private function getTargetEntityId($metadata)
    {
        $fields = array();
        
        foreach ($metadata->associationMappings as $fieldName => $relation) {
            if ($relation['type'] === ClassMetadataInfo::MANY_TO_MANY) {
                $fields['field_name'] = $fieldName;
                $fields['id'] = $relation['joinTable']['inverseJoinColumns'][0]['referencedColumnName'];
            }
        }
        
        return $fields;
    }
    
    private function getFieldsType($metadata)
    {
        $fields = array();
        
        $fieldsWithoutId = $metadata->isIdentifierNatural()
                ? $metadata->fieldMappings
                : array_slice($metadata->fieldMappings, 1, count($metadata->fieldMappings));
        
        foreach ($fieldsWithoutId as $fieldName => $relation) {
                $fields[$fieldName] = $this->getTypes($fieldName, $relation['type']);
        }
        
        return $fields;
    }
    
    private function getTypes($fieldName, $type)
    {
        $fields = array();
        
        $fields['name'] = $fieldName;
        $fields['type'] = $type;
        
        return $fields;
    }
    
    private function setIdentifier(ClassMetadataInfo $metadata)
    {
        if (count($metadata->identifier) == 1) {
            $this->identifier = $metadata->identifier[0];
        } elseif (empty($metadata->identifier[0]) && $metadata->associationMappings) {
            foreach ($metadata->associationMappings as $fieldName => $relation) {
                if ($relation['type'] === ClassMetadataInfo::ONE_TO_ONE) {
                    $this->identifier = $relation['joinColumns'][0]['name'];
                }
            }
        } else {
            throw new RuntimeException('The CRUD generator does not support entity classes with multiple or no primary keys.');
        }
        
        return $this;
    }
    
    private function getIdentifier()
    {
        return $this->identifier;
    }
}

