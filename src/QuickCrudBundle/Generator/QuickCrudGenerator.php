<?php

namespace QuickCrudBundle\Generator;

use Petkopara\CrudGeneratorBundle\Generator\PetkoparaCrudGenerator;
use Petkopara\CrudGeneratorBundle\Configuration\Configuration;
use Doctrine\Common\Util\Inflector;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use RuntimeException;
use Symfony\Component\HttpKernel\Bundle\BundleInterface;

class QuickCrudGenerator extends PetkoparaCrudGenerator
{
    protected $config;
    protected $fileUploadField;
    private $paginationData;
    private $recursiveAssocFields = array();
    private $isRecursiveAssoc = false;
    private $recursiveParent;
    private $recursiveChild;
    private $recursiveParentIdentifier;
    private $isLoginEntity = false;
    private $loginUsername;
    private $loginPassword;
    private $assocTargetEntity;
    private $assocField;
    private $identifier;
    private $assocEntityFields;
    /**
     * 
     * @param BundleInterface $bundle
     * @param string $entity
     * @param ClassMetadataInfo $metadata
     * @param Configuration $config
     * @throws RuntimeException
     */
    public function generateCrud(BundleInterface $bundle, $entity, ClassMetadataInfo $metadata, Configuration $config, $fileUploadField = null, $paginationData = null, $recursiveAssoc = null, $loginData = null, $assocEntityFields = null)
    {
        $this->config = $config;
        $this->routeNamePrefix = self::getRouteNamePrefix($config->getRoutePrefix());
        $this->actions = $config->getCrudActions();

        $this->setIdentifier($metadata);

        $this->entity = $entity;
        $this->entitySingularized = lcfirst(Inflector::singularize($entity));
        $this->entityPluralized = lcfirst(Inflector::pluralize($entity));
        $this->bundle = $bundle;
        $this->metadata = $metadata;
        $this->fileUploadField = $fileUploadField;
        $this->paginationData = $paginationData;
        
        if ($assocEntityFields) {
            $this->assocEntityFields = $assocEntityFields;
        }
        
        if ($loginData['isLoginEntity'] && !empty($loginData['login_username'])) {
            $this->isLoginEntity = true;
            $this->loginUsername = $loginData['login_username'];
            $this->loginPassword = $loginData['login_password'];
            
            $this->generateUserDecorator();
        }
        
        if (count($recursiveAssoc) > 1) {
            $this->isRecursiveAssoc = true;
            $this->recursiveAssocFields = array_reverse($this->getRecursiveAssocFields($recursiveAssoc));
            $this->recursiveParentIdentifier = $this->getRecursiveAssocFields($recursiveAssoc);
            $this->recursiveParentIdentifier = lcfirst(Inflector::classify($this->recursiveParentIdentifier[0]['identifier']));
            $recursiveRelatnField = $this->getRecursiveAssocFields($recursiveAssoc);
            $this->recursiveParent = $recursiveRelatnField[0]['field_name'];
            $this->recursiveChild = $recursiveRelatnField[1]['field_name'];
        }
        
        if (count($this->metadata->associationMappings) === 1) {
            $this->assocTargetEntity = $this->getAssoicateFields($this->metadata->associationMappings)['targetEntity'];
            $this->assocField = $this->getAssoicateFields($this->metadata->associationMappings)['assocField'];
        }
        
        $this->setFormat($config->getFormat());
        
        //save in bundle Resources
        $dir = sprintf('%s/Resources/views/%s', $bundle->getPath(), str_replace('\\', '/', $this->entity));
        
        $this->generateCrudControllerClass();

        if (!file_exists($dir)) {
            $this->filesystem->mkdir($dir, 0777);
        }

        $this->generateIndexView($dir);

        if (in_array('show', $this->actions)) {
            $this->generateShowView($dir);
        }

        if (in_array('new', $this->actions)) {
            $this->generateNewView($dir);
        }

        if (in_array('edit', $this->actions)) {
            $this->generateEditView($dir);
        }

        if (!empty($this->assocEntityFields) && key_exists(0, $this->assocEntityFields)) {
            $this->generateManyToManyTestClass();
        }
        
        $this->generateTestClass();
        $this->generateConfiguration();
        $this->generateValidationConfiguration();
        
        if ($this->isManyToMany($this->metadata) && $this->isOwingSide($this->metadata)) {
            $this->generateServiceConfiguration();
        }
    }
    
    /**
     * Generates the index.html.twig template in the final bundle.
     *
     * @param string $dir The path to the folder that hosts templates in the bundle
     */
    protected function generateIndexView($dir)
    {
        $this->renderFile('crud/views/index.html.twig.twig', $dir . '/index.html.twig', array(
            'bundle' => $this->bundle->getName(),
            'entity' => $this->entity,
            'entity_pluralized' => $this->entityPluralized,
            'entity_singularized' => $this->entitySingularized,
            'identifier' => $this->getIdentifier(),
            'fields' => $this->metadata->fieldMappings,
            'actions' => $this->actions,
            'record_actions' => $this->getRecordActions(),
            'route_prefix' => $this->config->getRoutePrefix(),
            'route_name_prefix' => $this->routeNamePrefix,
            'base_template' => $this->config->getBaseTemplate(),
            'without_bulk_action' => $this->config->getWithoutBulk(),
            'without_sorting' => $this->config->getWithoutSorting(),
            'without_page_size' => $this->config->getWithoutPageSize(),
            'file_upload_field' => $this->fileUploadField,
            'pagination' => $this->paginationData,
            'association' => count($this->metadata->associationMappings),
            'isOwingSide' => $this->isOwingSide($this->metadata),
            'isManyToMany' => $this->isManyToMany($this->metadata),
        ));
    }
    
    /**
     * Generates the show.html.twig template in the final bundle.
     *
     * @param string $dir The path to the folder that hosts templates in the bundle
     */
    protected function generateShowView($dir)
    {
        $this->renderFile('crud/views/show.html.twig.twig', $dir . '/show.html.twig', array(
            'bundle' => $this->bundle->getName(),
            'entity' => $this->entity,
            'entity_singularized' => $this->entitySingularized,
            'identifier' => $this->getIdentifier(),
            'fields' => $this->metadata->fieldMappings,
            'actions' => $this->actions,
            'route_prefix' => $this->config->getRoutePrefix(),
            'route_name_prefix' => $this->routeNamePrefix,
            'base_template' => $this->config->getBaseTemplate(),
            'file_upload_field' => $this->fileUploadField,
            'pagination' => $this->paginationData,
            'association' => count($this->metadata->associationMappings),
            'isOwingSide' => $this->isOwingSide($this->metadata),
            'isManyToMany' => $this->isManyToMany($this->metadata),
        ));
    }
    
    /**
     * Generates the new.html.twig template in the final bundle.
     *
     * @param string $dir The path to the folder that hosts templates in the bundle
     */
    protected function generateNewView($dir)
    {
        $this->renderFile('crud/views/new.html.twig.twig', $dir . '/new.html.twig', array(
            'bundle' => $this->bundle->getName(),
            'entity' => $this->entity,
            'entity_singularized' => $this->entitySingularized,
            'route_prefix' => $this->config->getRoutePrefix(),
            'route_name_prefix' => $this->routeNamePrefix,
            'actions' => $this->actions,
            'fields' => $this->metadata->fieldMappings,
            'base_template' => $this->config->getBaseTemplate(),
            'file_upload_field' => $this->fileUploadField,
            'pagination' => $this->paginationData,
            'association' => count($this->metadata->associationMappings),
            'isOwingSide' => $this->isOwingSide($this->metadata),
            'isManyToMany' => $this->isManyToMany($this->metadata),
        ));
    }
    
    /**
     * Generates the edit.html.twig template in the final bundle.
     *
     * @param string $dir The path to the folder that hosts templates in the bundle
     */
    protected function generateEditView($dir)
    {
        $this->renderFile('crud/views/edit.html.twig.twig', $dir . '/edit.html.twig', array(
            'route_prefix' => $this->config->getRoutePrefix(),
            'route_name_prefix' => $this->routeNamePrefix,
            'identifier' => $this->getIdentifier(),
            'entity' => $this->entity,
            'entity_singularized' => $this->entitySingularized,
            'fields' => $this->metadata->fieldMappings,
            'bundle' => $this->bundle->getName(),
            'actions' => $this->actions,
            'base_template' => $this->config->getBaseTemplate(),
            'file_upload_field' => $this->fileUploadField,
            'pagination' => $this->paginationData,
            'association' => count($this->metadata->associationMappings),
            'isOwingSide' => $this->isOwingSide($this->metadata),
            'isManyToMany' => $this->isManyToMany($this->metadata),
        ));
    }
    
    /**
     * Generates the controller class only.
     */
    protected function generateCrudControllerClass()
    {
        $dir = $this->bundle->getPath();

        $parts = explode('\\', $this->entity);
        $entityClass = array_pop($parts);
        $entityNamespace = implode('\\', $parts);

        $target = sprintf(
                '%s/Controller/%s/%sController.php', $dir, str_replace('\\', '/', $entityNamespace), $entityClass
        );

        if (!$this->config->getOverwrite() && file_exists($target)) {
            throw new \RuntimeException('Unable to generate the controller as it already exists.');
        }
        $fields = $this->getManyToManyFields($this->metadata);
                
        $assocEntityClass = $this->getEntityClass($this->assocTargetEntity);
        
        $this->renderFile('crud/controller.php.twig', $target, array(
            'actions' => $this->actions,
            'route_prefix' => $this->config->getRoutePrefix(),
            'route_name_prefix' => $this->routeNamePrefix,
            'bundle' => $this->bundle->getName(),
            'entity_bundle' => 'DataBundle',
            'entity' => $this->entity,
            'entity_singularized' => $this->entitySingularized,
            'entity_pluralized' => $this->entityPluralized,
            'entity_class' => $entityClass,
            'entityTableize' => Inflector::tableize($this->entity),
            'namespace' => $this->bundle->getNamespace(),
            'entity_namespace' => $entityNamespace,
            'format' => $this->config->getFormat(),
            'without_sorting' => $this->config->getWithoutSorting(),
            'without_page_size' => $this->config->getWithoutPageSize(),
            'bundle_views' => false,
            'identifier' => $this->getIdentifier(),
            'file_upload_field' => $this->fileUploadField,
            'pagination' => $this->paginationData,
            'assoc_many_to_many_fields' => $this->getManyToManyFields($this->metadata),
            'association' => count($this->metadata->associationMappings),
            'assocEntitySingular' => lcfirst($assocEntityClass),
            'assocEntityClass' => $assocEntityClass,
            'assocEntityTableize' => Inflector::tableize($assocEntityClass),
            'associate_field' => $this->assocField,
            'assocEntity' => $this->assocTargetEntity,
            'isOwingSide' => $this->isOwingSide($this->metadata),
            'isManyToMany' => $this->isManyToMany($this->metadata),
            'isRecursiveAssociation' => $this->isRecursiveAssoc,
            'recursiveAssocFields' => $this->recursiveAssocFields,
            'recursiveParent' => $this->recursiveParent,
            'recursiveChild' => $this->recursiveChild,
            'recursiveParentIdentifier' => $this->recursiveParentIdentifier,
            'isOneToOne' => $this->isOneToOne($this->metadata),
            'oneToOneAssocFields' => $this->getOneToOneAssocField($this->metadata),
        ));
    }
    
    /**
     * Generates the routing configuration.
     */
    protected function generateConfiguration()
    {
        if (!in_array($this->format, array('yml', 'xml', 'php'))) {
            return;
        }

        $target = sprintf(
            '%s/Resources/config/routing/%s.%s',
            $this->bundle->getPath(),
            strtolower(str_replace('\\', '_', $this->entity)),
            $this->format
        );
        $entityClass = $this->getEntityClass($this->assocTargetEntity);
        
        $this->renderFile('crud/config/routing.'.$this->format.'.twig', $target, array(
            'actions' => $this->actions,
            'route_prefix' => 'api',
            'route_name_prefix' => $this->routeNamePrefix,
            'bundle' => $this->bundle->getName(),
            'entity' => $this->entity,
            'entitySingular' => lcfirst($this->entity),
            'entityTableize' => Inflector::tableize($this->entity),
            'association' => count($this->metadata->associationMappings),
            'assocEntityClassSingular' => lcfirst($entityClass),
            'assocEntityClass' => $entityClass,
            'assocEntityTableize' => Inflector::tableize($entityClass),
            'associate_field' => ucfirst($this->assocField),
            'assocEntity' => $this->assocTargetEntity,
            'isOwingSide' => $this->isOwingSide($this->metadata),
            'isManyToMany' => $this->isManyToMany($this->metadata),
            'assoc_many_to_many_fields' => $this->getManyToManyFields($this->metadata),
            'isRecursiveAssociation' => $this->isRecursiveAssoc,
            'recursiveAssocFields' => $this->recursiveAssocFields,
            'isOneToOne' => $this->isOneToOne($this->metadata),
            'oneToOneAssocFields' => $this->getOneToOneAssocField($this->metadata),
        ));
    }
    
    protected function generateServiceConfiguration()
    {
        if (!in_array($this->format, array('yml', 'xml', 'php'))) {
            return;
        }
        $fields = $this->getTargetEntityName($this->metadata);
        
        $target = sprintf(
            '%s/Resources/config/services.%s',
            $this->bundle->getPath(),
            $this->format
        );
        
        $parts = explode('\\', $this->entity);
        $entityClass = array_pop($parts);
        $entityNamespace = implode('\\', $parts);
        
        $this->renderFile('crud/config/services.'.$this->format.'.twig', $target, array(
            'entity' => $this->entity,
            'entitySingular' => lcfirst($this->entity),
            'namespace' => $this->bundle->getNamespace(),
            'entity_namespace' => $entityNamespace,
            'target_entity_class' => $fields['target_entity_class'],
            'form_class' => $entityClass,
            'assoc_many_to_many_fields' => $this->getManyToManyFields($this->metadata),
        ));
        
    }

    protected function generateValidationConfiguration()
    {
        if (!in_array($this->format, array('yml', 'xml', 'php'))) {
            
            return;
        }
        
        $fieldsWithType = $this->getFieldsType($this->metadata);

        $target = sprintf(
            '%s/Resources/config/validation.%s',
            $this->bundle->getPath(),
            $this->format
        );

        $this->renderFile('crud/config/validation.'.$this->format.'.twig', $target, array(
            'entity' => $this->entity,
            'entity_bundle' => $this->metadata->name,
            'fields_with_types' => $fieldsWithType,
            'file_upload_field' => $this->fileUploadField,
        ));
    }
    
    protected function generateUserDecorator()
    {
        $dir = strstr($this->bundle->getPath(), 'src', true);
                
        $target = sprintf(
            '%ssrc/DataBundle/Entity/UserDecorator.php',
            $dir
        );
        
        $this->renderFile('crud/security/decorator/UserDecorator.php.twig', $target, array(
            'namespace' => $this->bundle->getNamespace(),
            'entity' => $this->entity,
            'entity_lowercase' => lcfirst($this->entity),
            'identifier' => ucfirst($this->getIdentifier()),
            'login_lookup_username' => $this->loginUsername,
            'login_lookup_password' => $this->loginPassword,
        ));
    } 
    
    protected function generateTestClass()
    {
        $parts = explode('\\', $this->entity);
        $entityClass = array_pop($parts);
        $entityNamespace = implode('\\', $parts);

        $dir = $this->bundle->getPath().'/Tests/Controller';
        $target = $dir.'/'.str_replace('\\', '/', $entityNamespace).'/'.$entityClass.'ControllerTest.php';

        $entityTableize = Inflector::tableize($this->entity);
        $assoEntityTableize = $this->getEntityClass($this->assocTargetEntity);

        $currentEntityFields = $this->assocEntityFields[$entityTableize];
        if (count($this->metadata->associationMappings) === 1) {
            $associateEntity = array_pop($currentEntityFields);
        }
                
        array_shift($this->assocEntityFields);
        
        if (count($this->assocEntityFields) > 1 && $associateEntity['type'] === ClassMetadataInfo::MANY_TO_ONE) {
            $recursiveAssocEntity = $associateEntity['class'];
            $recursiveAssocClass = Inflector::tableize($this->getEntityClass($recursiveAssocEntity));
            array_pop($this->assocEntityFields[$recursiveAssocClass]);
        }
        
        $assocEntityFields = array_reverse($this->assocEntityFields);

        $this->renderFile('crud/tests/test.php.twig', $target, array(
            'route_prefix' => $this->routePrefix,
            'route_name_prefix' => $this->routeNamePrefix,
            'entity' => $this->entity,
            'bundle' => $this->bundle->getName(),
            'entity_class' => $entityClass,
            'entity_class_tableize' => $entityTableize,
            'entity_class_lowercase' => lcfirst($entityClass),
            'namespace' => $this->bundle->getNamespace(),
            'entity_namespace' => $entityNamespace,
            'identifier' => Inflector::tableize($this->getIdentifier()),
            'actions' => $this->actions,
            'current_entity_fields' => $currentEntityFields,
            'assoc_entity_fields' => $assocEntityFields,
            'recursiveParent' => $this->recursiveParent,
            'recursiveChild' => $this->recursiveChild,
            'association' => count($this->metadata->associationMappings),
            'file_upload_field' => $this->fileUploadField,
            'isOwingSide' => $this->isOwingSide($this->metadata),
            'isManyToMany' => $this->isManyToMany($this->metadata),
            'isOneToOne' => $this->isOneToOne($this->metadata),
            'assoc_entity' => $assoEntityTableize,
            'assoc_entity_tableize' => Inflector::tableize($assoEntityTableize),
            'assoc_many_to_many_fields' => $this->getManyToManyFields($this->metadata),
            'form_type_name' => strtolower(str_replace('\\', '_', $this->bundle->getNamespace()).($parts ? '_' : '').implode('_', $parts).'_'.$entityClass),
        ));
    }
    
    protected function generateManyToManyTestClass()
    {
        $parts = explode('\\', $this->entity);
        $entityClass = array_pop($parts);
        $entityNamespace = implode('\\', $parts);
        
        $entityTableize = Inflector::tableize($this->entity);
        $currentEntityFields = $this->assocEntityFields[$entityTableize];
        
        if (key_exists(0, $this->assocEntityFields)) {
            $currentEntityFields = $this->removeAssociateFields($currentEntityFields);
            $assocEntityFields = $this->assocEntityFields[0];
        }
        
        $fields = $this->getManyToManyFields($this->metadata);

        foreach ($fields as $field) {
            $assocFields = array();
            $targetEntity = explode('\\', $field['class']);
            $targetEntityClass = array_pop($targetEntity);
            $manageMethod = $entityClass . $targetEntityClass;
            $targetEntityTableize = lcfirst(Inflector::tableize($targetEntityClass));

            $assocFields[$targetEntityTableize] = $assocEntityFields[$targetEntityTableize];
            
            $dir = $this->bundle->getPath().'/Tests/Controller';
            $target = $dir.'/'.str_replace('\\', '/', $entityNamespace).'/'.$manageMethod.'ControllerTest.php';
            
            $this->renderFile('crud/tests/manyToManyTest.php.twig', $target, array(
                'route_prefix' => $this->routePrefix,
                'route_name_prefix' => $this->routeNamePrefix,
                'entity' => $this->entity,
                'bundle' => $this->bundle->getName(),
                'entity_class' => $entityClass,
                'manage_method_name' => $manageMethod,
                'entity_class_tableize' => $entityTableize,
                'entity_class_lowercase' => lcfirst($entityClass),
                'namespace' => $this->bundle->getNamespace(),
                'entity_namespace' => $entityNamespace,
                'identifier' => Inflector::tableize($this->getIdentifier()),
                'field_name' => $field['name'],
                'current_entity_fields' => $currentEntityFields,
                'assoc_entity_fields' => $assocFields,
                'target_entity_class' => $targetEntityClass,
                'file_upload_field' => $this->fileUploadField,
                'association' => count($this->metadata->associationMappings),
                'isOwingSide' => $this->isOwingSide($this->metadata),
                'isManyToMany' => $this->isManyToMany($this->metadata),
                'recursiveParent' => $this->recursiveParent,
                'recursiveChild' => $this->recursiveChild,
            ));
        }
    }
    
    private function removeAssociateFields($entityFields)
    {
        foreach ($entityFields as $fieldName => $relation) {
            if ($relation['type'] === ClassMetadataInfo::MANY_TO_MANY) {
                unset($entityFields[$fieldName]);
            }
        }
        
        return $entityFields;
    }

    private function getAssoicateFields($metadata)
    {
        $fields = array();
        
        foreach ($metadata as $fieldName => $relation) {
            $fields['assocField'] = $fieldName;
            $fields['targetEntity'] = $relation['targetEntity'];
        }
        
        return $fields;
    }
    
    private function getEntityClass($entity)
    {
        $entities = str_replace("\\", " ", $entity);
        $entityPath = explode(" ", $entities);
        
        return array_pop($entityPath);
    }
    
    private function isOwingSide($metadata)
    {
        foreach ($metadata->associationMappings as $fieldName => $relation) {
            if ($relation['type'] === ClassMetadataInfo::MANY_TO_MANY && $relation['isOwningSide']) {
                return true;
            }
        }
    }
    
    private function isManyToMany($metadata)
    {
        foreach ($metadata->associationMappings as $fieldName => $relation) {
            if ($relation['type'] === ClassMetadataInfo::MANY_TO_MANY) {
                return true;
            } 
        }
    }
    
    private function getTargetEntityName($metadata)
    {
        $fields = array();
        
        foreach ($metadata->associationMappings as $fieldName => $relation) {
            if ($relation['type'] === ClassMetadataInfo::MANY_TO_MANY) {
                $fields['field_name'] = $fieldName;
                $fields['target_entity_class'] = ucfirst($fieldName);
                $fields['target_entity'] = $relation['targetEntity'];
            }
        }
        
        return $fields;
    }
    
    private function getManyToManyFields(ClassMetadataInfo $metadata)
    {
        $fields = array();
        
        foreach ($metadata->associationMappings as $fieldName => $relation) {
            if ($relation['type'] === ClassMetadataInfo::MANY_TO_MANY && $relation['isOwningSide']) {
                $fields[$fieldName] = $this->getRelationFieldData($fieldName, $relation, $relation['type']);
            }
        }

        return $fields;
    }
    
    private function getRelationFieldData($fieldName, $relation, $relationType)
    {
        $field = array();
        $field['name'] = $fieldName;
        $field['widget'] = 'EntityType::class';
        $field['class'] = $relation['targetEntity'];
        $targetEntity = explode("\\", $relation['targetEntity']);
        $targetEntity = array_pop($targetEntity);
        $field['target_entity_class'] = $targetEntity;
        $field['target_entity_tableize'] = Inflector::tableize($targetEntity);
        $field['type'] = $relationType;
        
        return $field;
    }
    
    private function getRecursiveAssocFields($recursiveAssoc)
    {
        $fields = array();
        
        foreach ($recursiveAssoc as $fieldName => $field) {
            array_push($fields, array(
                'field_name' => $field['name'],
                'target_entity_class' => $field['target_entity_class'],
                'target_entity_tableize' => Inflector::tableize($field['target_entity_class']),
                'identifier' => $field['identifier']
            ));
        }
        
        return $fields;
    }
    
    private function getFieldsType($metadata)
    {
        $fields = array();
        
        $fieldsWithoutId = array_slice($metadata->fieldMappings, 1, count($metadata->fieldMappings));
        
        foreach ($fieldsWithoutId as $fieldName => $relation) {
            $fields[$fieldName] = $this->getTypes($fieldName, $relation);
        }
        
        return $fields;
    }
    
    private function getTypes($fieldName, $relation)
    {
        $fields = array();
        
        $fields['name'] = $fieldName;
        $fields['type'] = $relation['type'];
        
        if ($fields['type'] === 'string' && array_key_exists('length', $relation)) {
            $fields['length'] = $relation['length'];
        }
        
        $fields['nullable'] = $relation['nullable'];
        
        return $fields;
    }
    
    private function setIdentifier(ClassMetadataInfo $metadata)
    {
        if (count($metadata->identifier) == 1) {
            $this->identifier = $metadata->identifier[0];
        } elseif (empty($metadata->identifier[0]) && $metadata->associationMappings) {
            foreach ($metadata->associationMappings as $fieldName => $relation) {
                if ($relation['type'] === ClassMetadataInfo::ONE_TO_ONE) {
                    $this->identifier = $relation['joinColumns'][0]['name'];
                }
            }
        } else {
            throw new RuntimeException('The CRUD generator does not support entity classes with multiple or no primary keys.');
        }
        
        return $this;
    }
    
    private function getIdentifier()
    {
        return $this->identifier;
    }
    
    private function isOneToOne(ClassMetadataInfo $metadata)
    {
        foreach ($metadata->associationMappings as $fieldName => $relation) {
            if ($relation['type'] === ClassMetadataInfo::ONE_TO_ONE) {
                return true;
            }
        }
    }
    
    private function getOneToOneAssocField(ClassMetadataInfo $metadata)
    {
        $fields = array();
        
        foreach ($metadata->associationMappings as $fieldName => $relation) {
            if ($relation['type'] === ClassMetadataInfo::ONE_TO_ONE) {
                $fields['field_name'] = $fieldName;
                $targetEntity = explode("\\", $relation['targetEntity']);
                $targetEntity = array_pop($targetEntity);
                $fields['target_entity'] = $targetEntity;
                $fields['target_entity_tablize'] = Inflector::tableize($targetEntity);
            }
        }
        
        return $fields;
    }
}