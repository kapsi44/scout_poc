<?php

namespace QuickCrudBundle\Command;

use Sensio\Bundle\GeneratorBundle\Command\AutoComplete\EntitiesAutoCompleter;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Petkopara\CrudGeneratorBundle\Command\CrudGeneratorCommand;
use Symfony\Component\Config\Definition\Exception\Exception;
use QuickCrudBundle\Generator\QuickCrudGenerator;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Sensio\Bundle\GeneratorBundle\Command\Validators;
use Petkopara\CrudGeneratorBundle\Configuration\ConfigurationBuilder;
use Petkopara\CrudGeneratorBundle\Generator\Guesser\MetadataGuesser;
use QuickCrudBundle\Generator\QuickFormGenerator;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\HttpKernel\Bundle\BundleInterface;
use Sensio\Bundle\GeneratorBundle\Model\Bundle;
use Doctrine\Bundle\DoctrineBundle\Mapping\DisconnectedMetadataFactory;
use Doctrine\Common\Inflector\Inflector;

class QuickCrudCommand extends CrudGeneratorCommand
{
    protected $formGenerator;
    private $recursiveAssociation = array();
    private $associateEntityFields = array();
    
    protected function configure()
    {
        $this
            ->setName('ngpropel:accelerate')
            ->setDescription('A NGPropel Accelerator to generate CRUD operations')
            ->setDefinition(array(
                new InputArgument('entity', InputArgument::OPTIONAL, 'The entity class name to initialize (shortcut notation)'),
                new InputOption('entity', '', InputOption::VALUE_REQUIRED, 'The entity class name to initialize (shortcut notation)'),
                new InputOption('route-prefix', 'r', InputOption::VALUE_REQUIRED, 'The route prefix'),
                new InputOption('template', 't', InputOption::VALUE_REQUIRED, 'The base template which will be extended by the templates', 'PetkoparaCrudGeneratorBundle::base.html.twig'),
                new InputOption('format', 'f', InputOption::VALUE_REQUIRED, 'The format used for configuration files (php, xml, yml, or annotation)', 'annotation'),
                new InputOption('overwrite', 'o', InputOption::VALUE_NONE, 'Overwrite any existing controller or form class when generating the CRUD contents'),
                new InputOption('bundle-views', 'b', InputOption::VALUE_NONE, 'Whether or not to store the view files in app/Resources/views/ or in bundle dir'),
                new InputOption('without-sorting', 'wsr', InputOption::VALUE_NONE, 'Whether or not have sorting in the index'),
                new InputOption('without-page-size', 'wps', InputOption::VALUE_NONE, 'Whether or not to show items per page select in the index'),
                new InputOption('without-bulk', 'wb', InputOption::VALUE_NONE, 'Whether or not to generate bulk actions'),
                new InputOption('without-write', 'ww', InputOption::VALUE_NONE, 'Whether or not to generate create, new and delete actions'),
                new InputOption('filter-type', 'ft', InputOption::VALUE_REQUIRED, 'What type of filtrations to be used. Multi search input, Form filter or none', 'form'),
                new InputOption('association-form', 'af', InputOption::VALUE_REQUIRED, 'Whether or not to generate association form type class'),
                new InputOption('file_upload_field', 'fu', InputOption::VALUE_REQUIRED, 'Specifies which field will be used to save the uploaded file information'),
                new InputOption('api-version', 'version', InputOption::VALUE_REQUIRED, 'Specifies versioning', 'v1'),
                new InputOption('login-entity', 'login', InputOption::VALUE_REQUIRED, 'Whether or not the entity have login credential'),
                new InputOption('login-lookup-username', 'llu', InputOption::VALUE_REQUIRED, 'Specified value identify which field is username in entity'),
                new InputOption('login-lookup-password', 'llp', InputOption::VALUE_REQUIRED, 'Specified value identify which field is password in entity'),
                
            ))
                ->setHelp(<<<EOT
The <info>%command.name%</info> command generates a CRUD based on a Doctrine entity.

The default command generate all actions: index and new, show update, and delete.

<info>php %command.full_name% --entity=DataBundle:Post --format=yml</info>

Using the --file_upload_field option specified field consider as a file field type in form class when generate CRUD.
                
Using the --api-version option to generate bundles based on given version.
                
Using the --login-entity option consider entity as a login credential. 
                
Using the --login-lookup-username option if --login-entity is true must specified login lookup username.

Using the --login-lookup-password option if --login-entity is true must specified login lookup password.

EOT
        );
    }

    protected function createGenerator($bundle = null)
    {
        return new QuickCrudGenerator(
                $this->getContainer()->get('filesystem'), 
                $this->getContainer()->getParameter('kernel.root_dir'));
    }
    
    protected function getSkeletonDirs(BundleInterface $bundle = null)
    {
        $skeletonDirs = array();
        if (isset($bundle) && is_dir($dir = $bundle->getPath() . '/Resources/QuickCrudBundle/skeleton')) {
            $skeletonDirs[] = $dir;
        }

        if (is_dir($dir = $this->getContainer()->get('kernel')->getRootdir() . '/Resources/QuickCrudBundle/skeleton')) {
            $skeletonDirs[] = $dir;
        }

        $skeletonDirs[] = $this->getContainer()->get('kernel')->locateResource('@QuickCrudBundle/Resources/skeleton');
        $skeletonDirs[] = $this->getContainer()->get('kernel')->locateResource('@QuickCrudBundle/Resources');

        return $skeletonDirs;
    }
    
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $questionHelper = $this->getQuestionHelper();
        $questionHelper->writeSection($output, 'Welcome to the Quick CRUD generator');
        
        // namespace
        $output->writeln(array(
            '',
            'This command helps you generate CRUD controllers and templates.',
            '',
            'First, give the name of the existing entity for which you want to generate a CRUD',
            '(use the shortcut notation like <comment>AcmeBlogBundle:Post</comment>)',
            '',
        ));

        if ($input->hasArgument('entity') && $input->getArgument('entity') != '') {
            $input->setOption('entity', $input->getArgument('entity'));
        }

        $question = new Question($questionHelper->getQuestion('The Entity shortcut name', $input->getOption('entity')), $input->getOption('entity'));
        $question->setValidator(array('Sensio\Bundle\GeneratorBundle\Command\Validators', 'validateEntityName'));


        $autocompleter = new EntitiesAutoCompleter($this->getContainer()->get('doctrine')->getManager());
        $autocompleteEntities = $autocompleter->getSuggestions();
        $question->setAutocompleterValues($autocompleteEntities);
        $entity = $questionHelper->ask($input, $output, $question);

        $input->setOption('entity', $entity);
        list($bundle, $entity) = $this->parseShortcutNotation($entity);

        try {
            $entityClass = $this->getContainer()->get('doctrine')->getAliasNamespace($bundle) . '\\' . $entity;
            $metadata = $this->getEntityMetadata($entityClass);
        } catch (Exception $e) {
            throw new \RuntimeException(sprintf('Entity "%s" does not exist in the "%s" bundle. You may have mistyped the bundle name or maybe the entity doesn\'t exist yet (create it first with the "doctrine:generate:entity" command).', $entity, $bundle));
        }
        
        // file_upload_field ?
        $fileUploadField = $input->getOption('file_upload_field');
        $output->writeln(array(
            '',
            'By default, the value is null',
            '',
        ));
        $question = new Question($questionHelper->getQuestion('Enter file/image field name', $input->getOption('file_upload_field')), $input->getOption('file_upload_field'));
        $fileUploadField = $questionHelper->ask($input, $output, $question);
        $input->setOption('file_upload_field', $fileUploadField);
        
        // login ?
        $loginEntity = $input->getOption('login-entity') ? true : false; //default false 
        $output->writeln(array(
            '',
            'By default, the generator not created any login credential informations'
            . 'If you given true it has generate the with login credential.',
            '',
        ));
        $question = new ConfirmationQuestion($questionHelper->getQuestion('Do you specify this entity as login credential', $loginEntity ? 'yes' : 'no', '?', $loginEntity), $loginEntity);
        $loginEntity = $questionHelper->ask($input, $output, $question);
        $input->setOption('login-entity', $loginEntity);
        
        // login.login_lookup_user ?
        $loginUsername = $input->getOption('login-lookup-username');
        $output->writeln(array(
            '',
            'Required, If entity have login credential provide which field name'
            . 'is username',
            '',
        ));
        $question = new Question($questionHelper->getQuestion('Please provide username field', $loginUsername), $loginUsername);
        $loginUsername = $questionHelper->ask($input, $output, $question);
        $input->setOption('login-lookup-username', $loginUsername);
        
        // login.login_lookup_password ?
        $loginPassword = $input->getOption('login-lookup-password');
        $output->writeln(array(
            '',
            'Required, If entity have login credential provide which field name'
            . 'is password',
            '',
        ));
        $question = new Question($questionHelper->getQuestion('Please provide password field', $loginPassword), $loginPassword);
        $loginPassword = $questionHelper->ask($input, $output, $question);
        $input->setOption('login-lookup-password', $loginPassword);
        
        // versioning ?
        $apiVersion = $input->getOption('api-version');
        $output->writeln(array(
            '',
            'By default, the version is V1.',
            'If you provide versioning it will generate based on versioning.',
            '',
        ));
        $question = new Question($questionHelper->getQuestion('Please provide version', $apiVersion), $apiVersion);
        $apiVersion = $questionHelper->ask($input, $output, $question);
        $input->setOption('api-version', $apiVersion);
        
        // write?
        $withoutWrite = $input->getOption('without-write') ? true : false; //default false
        $output->writeln(array(
            '',
            'By default, the generator creates all actions: list and show, new, update, and delete.',
            'You can also skip it and to generate only "list and show" actions:',
            '',
        ));
        $question = new ConfirmationQuestion($questionHelper->getQuestion('Do you want to skip generating of the "write" actions', $withoutWrite ? 'yes' : 'no', '?', $withoutWrite), $withoutWrite);
        $withoutWrite = $questionHelper->ask($input, $output, $question);
        $input->setOption('without-write', $withoutWrite);
        
        // association-form ?
        $associationType = $input->getOptions('association-form') ?: false;
        $output->writeln(array(
            '',
            'By default, It can not generate association form types',
            '',
        ));
        $question = new ConfirmationQuestion($questionHelper->getQuestion('Do you want to generate the association form type class', $associationType ? 'yes' : 'no', '?', $associationType), $associationType);
        $associationType = $questionHelper->ask($input, $output, $question);
        $input->setOption('association-form', $associationType);
        
        // template?
        $template = $input->getOption('template');
        $output->writeln(array(
            '',
            'By default, the created views extends the CrudGeneratorBundle::base.html.twig',
            'You can also set your template which the views to extend, for example base.html.twig ',
            '',
        ));
        $question = new Question($questionHelper->getQuestion('Base template for the views', $template), $template);
        $template = $questionHelper->ask($input, $output, $question);
        $input->setOption('template', $template);
        
        // format
        $format = $input->getOption('format');
        $output->writeln(array(
            '',
            'Determine the format to use for the generated CRUD.',
            '',
        ));
        $question = new Question($questionHelper->getQuestion('Configuration format (yml, xml, php, or annotation)', $format), $format);
        $question->setValidator(array('Sensio\Bundle\GeneratorBundle\Command\Validators', 'validateFormat'));
        $format = $questionHelper->ask($input, $output, $question);
        $input->setOption('format', $format);
        
        // route prefix
        $prefix = $this->getRoutePrefix($input, $entity);
        $output->writeln(array(
            '',
            'Determine the routes prefix (all the routes will be "mounted" under this',
            'prefix: /prefix/, /prefix/new, ...).',
            '',
        ));
        $prefix = $questionHelper->ask($input, $output, new Question($questionHelper->getQuestion('Routes prefix', '/' . $prefix), '/' . $prefix));
        $input->setOption('route-prefix', $prefix);
        
        // summary
        $output->writeln(array(
            '',
            $this->getHelper('formatter')->formatBlock('Summary before generation', 'bg=blue;fg=white', true),
            '',
            sprintf('You are going to generate a CRUD controller for "<info>%s:%s</info>"', $bundle, $entity),
            sprintf('Using the "<info>%s</info>" format.', $format),
            sprintf('Route prefix "<info>%s</info>" format.', $prefix),
            sprintf('Base template "<info>%s</info>".', $template),
            sprintf('With write "<info>%s</info>".', (!$withoutWrite) ? 'yes' : 'no'),
            '',
        ));
    }
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $questionHelper = $this->getQuestionHelper();

        if ($input->isInteractive()) {
            $question = new ConfirmationQuestion($questionHelper->getQuestion('Do you confirm generation', 'yes', '?'), true);
            if (!$questionHelper->ask($input, $output, $question)) {
                $output->writeln('<error>Command aborted</error>');
                return 1;
            }
        }

        $entity = Validators::validateEntityName($input->getOption('entity'));
        list($bundle, $entity) = $this->parseShortcutNotation($entity);
        
        //get the options
        $format = Validators::validateFormat($input->getOption('format'));
        $prefix = $this->getRoutePrefix($input, $entity);
        $withoutWrite = $input->getOption('without-write');
        $withoutBulk = $input->getOption('without-bulk');
        $withoutSorting = $input->getOption('without-sorting');
        $withoutPageSize = $input->getOption('without-page-size');
        $bundleViews = $input->getOption('bundle-views');
        $template = $input->getOption('template');
        $associateFormType = $input->getOption('association-form');
        $fileUploadField = $input->getOption('file_upload_field');
        $apiVersion = $input->getOption('api-version');
        $loginEntity = $input->getOption('login-entity');
        $loginUsername = $input->getOption('login-lookup-username');
        $loginPassword = $input->getOption('login-lookup-password');
        
        $forceOverwrite = $input->getOption('overwrite');

        $questionHelper->writeSection($output, 'CRUD generation');
        
        try {
            $entityClass = $this->getContainer()->get('doctrine')->getAliasNamespace($bundle) . '\\' . $entity;
            $metadata = $this->getEntityMetadata($entityClass);
        } catch (Exception $e) {
            throw new \RuntimeException(sprintf('Entity "%s" does not exist in the "%s" bundle. Create it with the "doctrine:generate:entity" command and then execute this command again.', $entity, $bundle));
        }
               
        if ($fileUploadField != null) {
            if (!in_array($fileUploadField, $metadata[0]->fieldNames)) {
                throw new \RuntimeException(sprintf('The given "%s" field does not exist in "%s" bundle', $fileUploadField, $entity));
            }
        }
        
        if (!(empty($loginUsername) && empty($loginPassword))) {
            
            if (!in_array($loginUsername, $metadata[0]->fieldNames)) {
                throw new \RuntimeException(sprintf('The given "%s" field does not exist in "%s" bundle', $loginUsername, $entity));
            }
            
            if (!in_array($loginPassword, $metadata[0]->fieldNames)) {
                throw new \RuntimeException(sprintf('The given "%s" field does not exist in "%s" bundle', $loginPassword, $entity));
            }
        }
        
        $this->setAssociateEntityFields($metadata[0]);
        
        $loginData = array(
            'isLoginEntity' => $loginEntity,
            'login_username' => $loginUsername,
            'login_password' => $loginPassword,
        );
            
        // Entity class names for associate forms
        $newBundle = $entity . 'Bundle';
        
        $shared = false;
        $namespace = Validators::validateBundleNamespace($newBundle, $shared);
        $bundleName = Validators::validateBundleName($newBundle);
        $formats = Validators::validateFormat('yml');
        $dir = 'src/';
        $projectRootDirectory = $this->getContainer()->getParameter('kernel.root_dir').'/..';

        if (!$this->getContainer()->get('filesystem')->isAbsolutePath($dir)) {
            $dir = $projectRootDirectory.'/'.$dir;
        }
        
        $generateBundle = new Bundle($namespace, $bundleName, $dir, $formats, $shared);
        if (!$shared) {
            $testsDir = $projectRootDirectory.'/tests/'.$bundleName;
            $generateBundle->setTestsDirectory($testsDir);
        }
        
        $errors = array();
        
        $output->writeln('Generating the Bundle code: <info>OK</info> ');
        
        $kernel = new \AppKernel('dev', true);
        $kernel->boot();
        
        $entityBundle = $kernel->getBundle($newBundle);
        
        $configBuilder = new ConfigurationBuilder();
        $configuration = $configBuilder
                ->setBaseTemplate($template)
                ->setBundleViews(false)
                ->setWithoutWrite($withoutWrite)
                ->setWithoutBulk($withoutBulk)
                ->setWithoutSorting($withoutSorting)
                ->setWithoutPageSize($withoutPageSize)
                ->setOverwrite($forceOverwrite)
                ->setFormat($format)
                ->setRoutePrefix($prefix)
                ->getConfiguration();
        
        $paginationData = $this->getPaginationDetails();
        $this->getParentAssoc($metadata[0]);
        
        $generator = $this->getGenerator($entityBundle); 
               
        $generator->generateCrud($entityBundle, $entity, $metadata[0], $configuration, $fileUploadField, $paginationData, $this->recursiveAssociation, $loginData, $this->getAssociateEntityFields());

        $output->writeln('Generating the CRUD code: <info>OK</info>');

        $runner = $questionHelper->getRunner($output, $errors);
        
        // form
        if ($withoutWrite === false) {
            $this->generateForm($entityBundle, $entity, $metadata, $forceOverwrite, $fileUploadField, $this->recursiveAssociation);
            $output->writeln('Generating the Form code: <info>OK</info>');
        }
        
        // associate-form-types
        if ($associateFormType === true) {
            $assocFields = $this->getAssoicateFields($metadata[0]);
            foreach ($assocFields as $fields) {
                $accoEntityClass = $this->getEntityClass($fields['entity']);
                try {
                    $assocEntity = $this->getContainer()->get('doctrine')->getAliasNamespace($bundle) . '\\' . $accoEntityClass;
                    $assocMetadata = $this->getEntityMetadata($assocEntity);
                    $this->generateForm($entityBundle, $fields['entity'], $assocMetadata, $forceOverwrite, $fileUploadField);
                } catch (Exception $e) {
                    throw new \RuntimeException(sprintf('Entity "%s" does not exist in the "%s" bundle. Create it with the "doctrine:generate:entity" command and then execute this command again.', $accoEntityClass, $bundle));
                }
            }
                
            $output->writeln('Generating the Associate Form Type class code: <info>OK</info>');
        }
        $routePrefix = 'api/' . $apiVersion;
        // routing
        $output->write('Updating the routing: ');
        if ('annotation' != $format) {
            $runner($this->updateRouting($questionHelper, $input, $output, $entityBundle, $format, $entity, $routePrefix));
        } else {
            $runner($this->updateAnnotationRouting($entityBundle, $entity, $prefix));
        }

        $questionHelper->writeGeneratorSummary($output, $errors);
    }
    
    protected function generateForm($bundle, $entity, $metadata, $forceOverwrite = false, $fileUploadField = null)
    {
        $this->getFormGenerator($bundle)->generate($bundle, $entity, $metadata[0], $forceOverwrite, $fileUploadField);
    }

    protected function getFormGenerator($bundle = null)
    {
        if (null === $this->formGenerator) {
            $metadataGuesser = new MetadataGuesser(new DisconnectedMetadataFactory($this->getContainer()->get('doctrine')));
            $this->formGenerator = new QuickFormGenerator($metadataGuesser);
            $this->formGenerator->setSkeletonDirs($this->getSkeletonDirs($bundle));
        }

        return $this->formGenerator;
    }
    
    private function getAssoicateFields(ClassMetadataInfo $metadata)
    {
        $fields = array();
        
        foreach ($metadata->associationMappings as $fieldName => $relation) {
            switch ($relation['type']) {
                case ClassMetadataInfo::ONE_TO_MANY:
                    if ($relation['mappedBy']) {
                        $fields[$fieldName] = $this->getTargetEntity($relation);
                    }
            }
        }
        
        return $fields;
    }
    
    private function getTargetEntity($relation)
    {
        $field = array();
        
        $field['entity'] = $relation['targetEntity'];
        
        return $field;
    }
    
    private function getEntityClass($entity)
    {
        $entities = str_replace("\\", " ", $entity);
        $entityPath = explode(" ", $entities);
        
        return array_pop($entityPath);
    }
    
    private function getPaginationDetails()
    {
        $data = array(
            'limit' => $this->getContainer()->getParameter('limit'),
        );
         
        return $data;
    }
    
    private function getParentAssoc(ClassMetadataInfo $metadata)
    {
        $tableName = $metadata->table['name'];
        if (count($metadata->associationMappings) === 1) {
            foreach ($metadata->associationMappings as $fieldName => $relation) {
                $fieldName = $tableName . '_' . $fieldName;
                if (($relation['type'] === ClassMetadataInfo::MANY_TO_ONE)) {
                    $this->recursiveAssociation[$fieldName] = $this->getRelationFieldData($fieldName, $relation);
                }
            }
            if (!empty($this->recursiveAssociation)) {
                foreach ($this->recursiveAssociation as $field) {
                    $targetClassMetadata = $this->getAssocEntityMetadata($field['class']);
                }

                return $this->getParentAssoc($targetClassMetadata[0]);
            }
        }
    }
    
    private function getRelationFieldData($fieldName, $relation)
    {
        $field = array();
        $field['identifier'] = $relation['joinColumns'][0]['referencedColumnName'];
        $fieldName = explode('_', $fieldName);
        $field['name'] = array_pop($fieldName);
        $field['widget'] = 'EntityType::class';
        $field['class'] = $relation['targetEntity'];
        $targetEntity = explode("\\", $relation['targetEntity']);
        $field['target_entity_class'] = array_pop($targetEntity);
                
        return $field;
    }
    
    private function getAssocEntityMetadata($entity)
    {
        return $this->getEntityMetadata($entity);
    }
    
    private function setAssociateEntityFields(ClassMetadataInfo $metadata)
    {
        $tableName = $metadata->table['name'];
        
        $this->associateEntityFields[$tableName] = $this->getEntityFields($metadata);

        if ($metadata->associationMappings && !empty($this->associateEntityFields)) {
            if (count($metadata->associationMappings) === 1 && $this->isManyToMany($metadata) === null) {
                foreach ($this->associateEntityFields as $tableName => $columns) {
                    $targetClassMetadata = $this->getMetadataOrFields($metadata, $columns);
                }

                if (!empty($targetClassMetadata)) {

                    return $this->setAssociateEntityFields($targetClassMetadata[0]);
                }
            }
            
            if ($this->isManyToMany($metadata) && $this->isOwingSide($metadata)) {
                foreach ($this->associateEntityFields as $tableName => $columns) {
                    $manyToManyFields = $this->getMetadataOrFields($metadata, $columns);
                    array_push($this->associateEntityFields, $manyToManyFields);
                }
            }
        }
    }
    
    private function getAssociateEntityFields()
    {
        return $this->associateEntityFields;
    }

    private function getMetadataOrFields(ClassMetadataInfo $metadata, $columns)
    {
        $data = array();
        
        foreach ($columns as $fieldName => $relation) {
            if (key_exists('class', $relation) && count($metadata->associationMappings) === 1) {
                $data = $this->getAssocEntityMetadata($relation['class']);
            }
            
            if (key_exists('class', $relation) && $relation['type'] === ClassMetadataInfo::MANY_TO_MANY) {
                $targetClassMetadata = $this->getAssocEntityMetadata($relation['class']);
                $data[$targetClassMetadata[0]->table['name']] = $this->getEntityFields($targetClassMetadata[0]);
            }
        }
        
        return $data;
    }
    
    private function getTypes($fieldName, $relation, $identifier, $isRelation = false)
    {
        $fields = array();
        
        $fields['identifier'] = Inflector::tableize($identifier);
        $fields['name'] = $fieldName;
        $fields['type'] = $relation['type'];
        
        if ($fields['type'] === 'string') {
            if (array_key_exists('length', $relation)) {
                $fields['length'] = $relation['length'];
                $fields['value'] = array_key_exists('default', $relation['options'])
                        ? $relation['options']['default']
                        : $this->generateString($fields['length']);
            } else {
                $fields['value'] = array_key_exists('default', $relation['options'])
                        ? $relation['options']['default']
                        : $this->generateString();
            }
        }
        
        if ($fields['type'] === 'text') {
            $fields['value'] = $this->generateString(50);
        }
        
        if (!$isRelation) {
            $fields['nullable'] = $relation['nullable'];
        }

        if ($relation['type'] === ClassMetadataInfo::MANY_TO_ONE
                || $relation['type'] === ClassMetadataInfo::MANY_TO_MANY
                || $relation['type'] === ClassMetadataInfo::ONE_TO_ONE
                ) {
            $fields['class'] = $relation['targetEntity'];
        }
        
        return $fields;
    }
    
    private function generateString($length = 10)
    {
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        
        for ($count = 0; $count < $length; $count++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        
        return $randomString;
    }
    
    private function getEntityFields(ClassMetadataInfo $metadata)
    {
        $fields = array();
        
        // Remove the primary key field if it's not managed manually
        $fieldsWithoutId = $metadata->isIdentifierNatural()
                ? $metadata->fieldMappings
                : array_slice($metadata->fieldMappings, 1, count($metadata->fieldMappings));
        
        foreach ($fieldsWithoutId as $fieldName => $relation) {
            $fields[$fieldName] = $this->getTypes($fieldName, $relation, $this->getIdentifier($metadata));
        }
        
        foreach ($metadata->associationMappings as $fieldName => $relation) {
            if ($relation['type'] === ClassMetadataInfo::MANY_TO_ONE) {
                $fields[$fieldName] = $this->getTypes($fieldName, $relation, $this->getIdentifier($metadata), true);
            }
            if ($relation['type'] === ClassMetadataInfo::MANY_TO_MANY) {
                $fields[$fieldName] = $this->getTypes($fieldName, $relation, $this->getIdentifier($metadata), true);
            }
            if ($relation['type'] === ClassMetadataInfo::ONE_TO_ONE) {
                $fields[$fieldName] = $this->getTypes($fieldName, $relation, $this->getIdentifier($metadata), true);
            }
        }
        
        return $fields;
    }
    
    private function isManyToMany($metadata)
    {
        foreach ($metadata->associationMappings as $fieldName => $relation) {
            if ($relation['type'] === ClassMetadataInfo::MANY_TO_MANY) {
                return true;
            } 
        }
    }
    
    private function isOwingSide($metadata)
    {
        foreach ($metadata->associationMappings as $fieldName => $relation) {
            if ($relation['type'] === ClassMetadataInfo::MANY_TO_MANY && $relation['isOwningSide']) {
                return true;
            }
        }
    }

    private function getIdentifier(ClassMetadataInfo $metadata)
    {
        if (count($metadata->identifier) == 1) {

            return $metadata->identifier[0];
        } elseif (empty($metadata->identifier[0]) && $metadata->associationMappings) {
            foreach ($metadata->associationMappings as $fieldName => $relation) {
                if ($relation['type'] === ClassMetadataInfo::ONE_TO_ONE) {

                    return $relation['joinColumns'][0]['name'];
                }
            }
        }

        throw new RuntimeException('The CRUD generator does not support entity classes with multiple or no primary keys.');
    }
}
