<?php

namespace CoreBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;

abstract class CoreController extends FOSRestController
{
    private $em = null;
    
    /**
     * 
     * @param type $dataSet - Entity object / Form object to render response or form error.
     * @param type $message - In case of custom messages, $dataSet will be null and $message will be used.
     * @param type $code -  HTTP status code.
     */
    protected function renderResponse($dataSet, $message = null, $code = null)
    {
        $view = View::create();
        $respData = empty($dataSet) ? $message : $dataSet;
        
        $view->setData($respData); 
        if ($code) {
            $view->setStatusCode($code);
        }
        
        return $view;
    }
    
    protected function getEntityManger()
    {
        if ($this->em === null) {
            $this->setEntityManager();
        }
        
        return $this->em;
    }
    
    protected function setEntityManager($em = null)
    {
        $this->em = $em ? $em : $this->getDoctrine()->getManager();
    }
        
    protected function sendError($formError)
    {
        $errors = '';
        
        foreach ($formError as $error) {
            $errors .= $error->getMessage();
            break;
        }
        
        return $this->renderResponse(null, $errors, 400);
    }
}
