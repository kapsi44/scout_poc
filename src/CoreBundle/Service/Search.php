<?php

namespace CoreBundle\Service;

use Doctrine\ORM\EntityManager;

class Search
{
    private $em;
    private $repository;
    private $maxRecordPerPage;
    private $currentPage;

    /**
     *
     * @param EntityManager $em  Entity Manager
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Method is used to search based on query request and return response
     * with pagination.
     *
     * @param string  $repository       Entity repository name
     * @param array   $searchData       Request query and Associate entity id's, represent the
     *                                  many to one/recursive many to one route id.
     * @param integer $currentPage      Current page
     * @param integer $maxRecord        Maximum record to display in a page
     * @param boolean $includePageInfo  To display page information like starting and total record
     *
     * @return array  $searchInfo
     */
    public function search($repository, $searchData, $currentPage, $maxRecord, $includePageInfo = true)
    {
        $this->repository = $repository;
        $this->maxRecordPerPage = $maxRecord;
        $this->currentPage = $currentPage;
        
        return $this->doSearch($searchData, $includePageInfo);
    }

    /**
     * @param array   $searchData       Request query and Associate entity id's, represent the
     *                                  many to one/recursive many to one route id.
     * @param boolean $includePageInfo  If true include display page informations like starting and total record
     *                                  in response else ignore informations.
     *
     * @return array $records Return response without page information details.
     *         array $dataSet  Return response with page information details.
     */
    private function doSearch($searchData, $includePageInfo)
    {
        $dataSet = array();
        
        $query = isset($searchData['query']) ? $searchData['query'] : null;
        $searchField = $this->getSearchFields($query);
        
        /* $searchData['assocEntity'], which have array of associate name as key and 
         * associate id as value. If index action have associate entity id, 
         * based on it find the result. */
        $assocEntity = isset($searchData['assocEntity']) ? $searchData['assocEntity'] : array();
        $fields = $assocEntity
                ? array_merge($assocEntity, $searchField)
                : $searchField;

        $records = $this->em
                    ->getRepository($this->repository)
                    ->findBy($fields, array(), $this->maxRecordPerPage, $this->getPageOffset());
        
        if (!$includePageInfo) {
            
            return $records;
        }
        
        $entity = explode(":", $this->repository);
        $entity = isset($entity[1]) ? $entity[1] : null;

        $totalRecord = (int) $this->getTotalRecordCount($entity);

        $dataSet['starting_record'] = $totalRecord ? $this->getPageOffset() + 1 : 0;
        $dataSet['total_record'] = $totalRecord;
        $dataSet['results'] = $records;
        
        return $dataSet;
    }
    
    /**
     * Method is used to get search key and value using array_map and combine
     * array key and values
     *
     * @param string $query Search query string value.
     *
     * @return array Combined search key and search value.
     */
    private function getSearchFields($query)
    {
        $delimiter = $this->getQueryDelimiter($query);
        $searchQuery = $query ? explode($delimiter, $query) : array();
        
        $searchField = array();
        
        array_walk(
            $searchQuery,
            function ($value, $key) use (&$searchField) {
                $searchData = explode("=", $value);
                $searchField[$searchData[0]] = $searchData[1];
            }
        );

        return $searchField;
    }
    
    /**
     * Method is used to check search conditions whether AND, OR, etc.,
     *
     * @param string $query Search query string value.
     *
     * @return delimiter
     */
    private function getQueryDelimiter($query)
    {
        if (preg_match('/[;]/', $query)) {
            // condition for AND operator
            return ';';
        } elseif (preg_match('/[|]/', $query)) {
            // condition for OR operator
            // return '|';
        } else {
            // condition for Single query search
            // For converting string to array I just pass '-' it.
            return '-';
        }
    }

    /**
     *
     * @param  string $entity Entity/Model name
     *
     * @return string Total record count of entity object.
     */
    private function getTotalRecordCount($entity)
    {
        $qb = $this->em
                ->createQueryBuilder()
                ->select('COUNT(' . $entity . ')')
                ->from($this->repository, $entity);

        return $qb->getQuery()->getSingleScalarResult();
    }
    
    /**
     *
     * @param integer $limit Limit to display the number of records.
     * @param integer $page  Current page.
     *
     * @return  integer   Page offset
     */
    private function getPageOffset()
    {
        return ($this->currentPage * $this->maxRecordPerPage) - $this->maxRecordPerPage;
    }
}
