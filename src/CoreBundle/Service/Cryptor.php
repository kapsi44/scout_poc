<?php
namespace CoreBundle\Service;

class Cryptor 
{
    const method = 'AES-128-CTR';
    
    public function encrypt($data, $salt)
    {
        $iv = openssl_random_pseudo_bytes($this->ivBytes());
        
        $encryptedToken = bin2hex($iv) . openssl_encrypt($data, self::method, $salt, 0, $iv);

        return $encryptedToken;
    }

    public function decrypt($token, $salt)
    {
        $ivStrlen = 2  * $this->ivBytes();
        
        if (preg_match("/^(.{" . $ivStrlen . "})(.+)$/", $token, $regs)) {
            list(, $iv, $encryptedToken) = $regs;
            
            $decrypted = openssl_decrypt($encryptedToken, self::method, $salt, 0, hex2bin($iv));

            return $decrypted;
        }
    }
    
    private function ivBytes()
    {
        return openssl_cipher_iv_length(self::method);
    }
}

