<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace CoreBundle\Service;

Class Mailer {

    protected $mailer;
    protected $mailFrom;

    public function __construct($mailer, $mailerFrom)
    {
        $this->mailer = $mailer;
        $this->mailFrom = $mailerFrom;
    }

    public function send($subject, $body, $to,  $from = null)
    {
        $fromEmail = $from ? $from : $this->mailFrom;
        $message = \Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom($fromEmail)
                ->setTo($to)
                //->setBody($this->renderView('HelloBundle:Hello:email.txt.twig', array('name' => $name)))
                ->setBody($body)
        ;

        $this->mailer->send($message);
    }

}
