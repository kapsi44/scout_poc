<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace CoreBundle\EventSubscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;

class WatchEntitySubscriber implements EventSubscriber {

    private $mailer = null;

    public function __construct($mailer)
    {
        $this->mailer = $mailer;
    }

    public function getSubscribedEvents()
    {
        return array(
            'postPersist',
        );
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $this->handle($args);
    }

    public function handle(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        //$this->mailer->send('Test Email', 'Test response body', 'test@test.com');
    }

}
