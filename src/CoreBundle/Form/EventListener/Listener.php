<?php

namespace CoreBundle\Form\EventListener;

use Symfony\Component\HttpKernel\Event\KernelEvent;
use Symfony\Component\HttpFoundation\ParameterBag;

class Listener
{
    public function onKernelRequest(KernelEvent $event)
    {
        $result = array();
        $request = $event->getRequest();
        if ($request->headers->get('content-type') === "application/json") {
            $result = $this->parseJsonParameter($request->getContent());
        }
                
        $request->request = new ParameterBag(array_merge($request->request->all(), $result));
    }
    
    public function parseJsonParameter($content)
    {
        $result = array();
        
        $data = json_decode($content, true);

        if ($data) {
            foreach ($data as $key => $value) {
                $result[$key] = $value;
            }
        }
        
        return $result;
    }
}