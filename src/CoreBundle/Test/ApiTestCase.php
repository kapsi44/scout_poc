<?php

namespace CoreBundle\Test;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use DataBundle\Entity\UserDecorator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

abstract class ApiTestCase extends WebTestCase
{
    protected $client;
    private $token;
    private $entityManager;
    private static $id;
    
    public function setUp()
    {
        $this->client = static::createClient();
        self::bootKernel();
        
        $this->setToken();
    }
    
    protected function setId($id)
    {
        self::$id = $id;

        return $this;
    }
    
    protected function getId()
    {
        return self::$id;
    }
    
    protected function get($id)
    {
        return static::$kernel->getContainer()->get($id);
    }
    
    protected function getParameter($name)
    {
        return static::$kernel->getContainer()->getParameter($name);
    }
        
    protected function getEntityManager()
    {            
        if (null === $this->entityManager) {
            $this->setEntityManager();
        }
        
        return $this->entityManager;
    }
     
    protected function setEntityManager($em = null)
    {
        $this->entityManager = $em 
                ? $em 
                : $this->get('doctrine')->getManager();
    }
    
    protected function getToken()
    {        
        return $this->token;
    }

    protected function setToken($username = null)
    {
        $login = $this->getEntityManager()
                ->getRepository($this->getParameter('login.repository'))
                ->findOneBy(array());
        
        $this->assertGreaterThanOrEqual(1, count($login), "Login - Empty object return, must have at least one user");
        
        $user = new UserDecorator($login);
        
        $username = $username ? $username : $user->getUserName();
        
        $token = $this->get('lexik_jwt_authentication.encoder')
                ->encode(['username' => $username]);
        
        $this->token = 'Bearer ' . $token;
    }
    
    /**
     * Assert the success response 
     * 
     * @param Response $response
     * @param type $message
     */
    protected function assertSuccessCode(Response $response, $message = null)
    {
        $this->assertEquals(200, $response->getStatusCode(), $message);
    }
    
    /**
     * Assert the Not found exception 
     * 
     * @param Response $response
     * @param type $message
     */
    protected function assertNotFoundException(Response $response, $message = null)
    {
        $this->assertEquals(404, $response->getStatusCode(), $message);
    }
    
    /**
     * Assert the response is Equal to expected values. 
     * 
     * @param Response $response
     * @param type $expectedValue
     */
    protected function assertResponsePropertyEquals(Response $response, $expectedValue)
    {
        $actual = $response->getContent();
        
        $this->assertContains(
            $expectedValue,
            $actual,
            sprintf(
                'Expected "%s" but the response was "%s"',
                $expectedValue,
                var_export($actual, true)
            )
        );
    }
    
    /**
     * Convert response content into array.
     * 
     * @param Response $response
     * @return type
     */
    protected function getResponseInArray(Response $response)
    {
        $data = json_decode ($response->getContent(), true);
        
        return $data;
    }
    
    /**
     * To generate URL for given routes.
     * 
     * @param type $route
     * @param type $parameters
     * @param type $referenceType
     * @return type
     */
    protected function generateUrl($route, $parameters = array(), $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH)
    {
        return $this->get('router')->generate($route, $parameters, $referenceType);
    }
    
    /**
     * Request http based on method and URI with JWT token.
     * 
     * @param type $method
     * @param type $uri
     * @param type $parameters
     * @param type $files
     * @param type $server
     * @param type $content
     * @param type $changeHistory
     * @return type
     */
    protected function doRequest($method, $uri, $parameters = array(), $files = array(), $server = array(), $content = null, $changeHistory = true)
    {
        return $this->client
                ->request($method,  $uri,  $parameters,  $files, array(
                    'HTTP_AUTHORIZATION' => $this->getToken()
                ));
    }
    
    /**
     * Get normal fields alone from response data.
     * To Fix - Removing association fields.
     * 
     * @param Response $response
     * @param type $fieldWithValues
     * @return type
     */
    protected function getFields(Response $response, $fieldWithValues)
    {
        $data = $this->getResponseInArray($response);
        
        $filter = array_filter($data, function($value, $key){
            if (!is_array($value)) {
                return $key && $value;
            }
        }, ARRAY_FILTER_USE_BOTH);
        
        $responseData = array_intersect_key($fieldWithValues, $filter);
        
        return $responseData;
    }
    
    /**
     * Assert the response data match with given input data.
     * 
     * @param Response $response
     * @param type $fieldWithValues
     */
    protected function assertResponseMatch(Response $response, $fieldWithValues, $message = null)
    {
        $message = $message ? $message : 'The response data mismatched with given input data';
        
        $responseData = $this->getFields($response, $fieldWithValues);
        
        /* Assert the given values are updated correctly by comparing with 
         * response data */
        $this->assertEquals(
                0, 
                count(array_diff($responseData, $fieldWithValues)),
                $message
                );
    }
}