<?php
namespace v1\StudentsBundle\Service;

use DataBundle\Entity\UserDecorator;

class Cryptor 
{
    private $student;

    public function encodeDecode_fields($object, $fields, $action)
    {
        if(in_array('name', $fields)) {
            $object->setName(UserDecorator::base64_encodeDecode($object->getName(), $action));
        } 
        
        if(in_array('emailid', $fields)) {
            $object->setEmailid(UserDecorator::base64_encodeDecode($object->getEmailid(), $action));
        } 
        
        if(in_array('dob', $fields)) {
            $object->setDob(UserDecorator::base64_encodeDecode($object->getDob(), $action));
        }

        return $object;
    }
}