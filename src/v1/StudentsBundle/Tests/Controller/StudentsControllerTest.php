<?php

namespace v1\StudentsBundle\Tests\Controller;

use CoreBundle\Test\ApiTestCase;

class StudentsControllerTest extends ApiTestCase
{
    private function studentsDetails()
    {
        $studentsData = array(
            'name' => 'oETdZWjMtiWiBarXBaUgPRnHaKfNvqOcfOWggEPeoDcIDlhSgQKAKEZktydeWPoLXllnpzxrDnpVqkpayjujHmaBzkbiZvkyHFUU',
            'emailid' => 'teeLmWvOUlKuCyAeQhEbAFlcPcIqyUmtXxMQCVtFUDdRyJiHmKtoVWLocVmmgHbZtwEwhIlOvYQXfbPbfvUTpsHIVmANZzhANtyo',
            'dob' => 'ZIJwFSRStPQQLEdQUykpnGCZnkyfVyVcsykZVdCKvTUMFKUWJWAlbJHupMkJLeHvcGjShHOUJZZIjTRPTYLdCUKQhjgqzKMKVkdV',
            'grade' => 'oCKOhDKOiK',
            'status' => 'JDQHkCUuEY',
            'createddate'=> '1995-05-05',

        );       
        
        return $studentsData;
    }
    
    public function testIndexAction()
    {
        $url = $this->generateUrl('students');

        $crawler = $this->doRequest('GET', $url);
        $this->assertSuccessCode($this->client->getResponse(), "Unexpected HTTP status code for index Students GET actions");
    
    }
    
    public function testNewAction()
    {
        $url = $this->generateUrl('students_new');
        $fieldWithValues = $this->studentsDetails();
        $crawler = $this->doRequest('POST', $url, $fieldWithValues);
        $this->assertSuccessCode($this->client->getResponse(), "Unexpected HTTP status code for new Students POST action");
        
        $studentsResult = $this->getResponseInArray($this->client->getResponse());
        $this->setId($studentsResult['id']);
        
    }
    
    public function testShowAction()
    {
        $url = $this->generateUrl('students_show', array(
            'id' => $this->getId()
        ));

        $fieldWithValues = $this->studentsDetails();
        $crawler = $this->doRequest('GET', $url);
        
        $this->assertSuccessCode($this->client->getResponse(), "Unexpected HTTP status code for show Students GET action");
        
        $this->assertResponseMatch($this->client->getResponse(), $fieldWithValues);
    }
    
    public function testEditAction()
    {
        $url = $this->generateUrl('students_edit', array(
            'id' => $this->getId()
        ));

        $fieldWithValues = $this->studentsDetails();
        $fieldWithValues['name'] = 'UUFHykvZibkzBamHjujyapkqVpnDrxzpnllXLoPWedytkZEKAKQgShlDIcDoePEggWOfcOqvNfKaHnRPgUaBXraBiWitMjWZdTEo';
        $fieldWithValues['emailid'] = 'oytNAhzZNAmVIHspTUvfbPbfXQYvOlIhwEwtZbHgmmVcoLWVotKmHiJyRdDUFtVCQMxXtmUyqIcPclFAbEhQeAyCuKlUOvWmLeet';
        $fieldWithValues['dob'] = 'VdkVKMKzqgjhQKUCdLYTPRTjIZZJUOHhSjGcvHeLJkMpuHJblAWJWUKFMUTvKCdVZkyscVyVfyknZCGnpkyUQdELQQPtSRSFwJIZ';
        $fieldWithValues['grade'] = 'KiOKDhOKCo';
        $fieldWithValues['status'] = 'YEuUCkHQDJ';
        $fieldWithValues['createddate'] = '1997-07-07';
        $crawler = $this->doRequest('POST', $url, $fieldWithValues);
        $this->assertSuccessCode($this->client->getResponse(), "Unexpected HTTP status code for update Students POST action");
        
        $url = $this->generateUrl('students_show', array(
            'id' => $this->getId()
        ));

        
        $crawler = $this->doRequest('GET', $url);
        
        $this->assertSuccessCode($this->client->getResponse(), "Unexpected HTTP status code for Students GET action");
        
        $this->assertResponseMatch($this->client->getResponse(), $fieldWithValues);
    }
    
    public function testDeleteAction()
    {
        $url = $this->generateUrl('students_delete', array(
            'id' => $this->getId()
        ));

        $crawler = $this->doRequest('DELETE', $url);
        
        $this->assertSuccessCode($this->client->getResponse(), "Unexpected HTTP status code for DELETE");

        $url = $this->generateUrl('students_show', array(
            'id' => $this->getId()
        ));

        
        $crawler = $this->doRequest('GET', $url);
        
        $this->assertNotFoundException($this->client->getResponse(), "Unexpected HTTP status code for Students GET after delete action");
    }
    
}
