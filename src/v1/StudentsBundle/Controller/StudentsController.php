<?php

namespace v1\StudentsBundle\Controller;

use CoreBundle\Controller\CoreController;
use DataBundle\Entity\Students;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;

/**
 * Students controller.
 *
 */
class StudentsController extends CoreController
{
    /**
     * Method used to list all Students entities.
     *
     * If limit doesn't given, the default limit 5
     * has taken.
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Method used to list all Students entities.",
     * )
     *
     */
    public function indexAction(Request $request)
    {
        $em = $this->getEntityManger();
        $limit = $request->get('limit', $this->getParameter('limit'));
        $currentPage = $request->get('page', 1);

        try {
            $searchData['query'] = $request->query->get('query');

            // $student = $this->get('core.search')
            //         ->search('DataBundle:Students', $searchData, $currentPage, $limit);

            $student = $em
                ->getRepository('DataBundle:Students')
                ->decodeFieldsData();
            return $student;
            // $student = $this->get('student.cryptor')
            //         ->decrypt_array_fields($student, ['name','emailid','dob']);

            if (!$student) {
                return $this->renderResponse(null, 'Students information not available', 404);
            } else {
                return $this->renderResponse($student);
            }
        } catch (Exception $ex) {
            return $this->renderResponse(
                null,
                array(
                    'message' => $ex->getMessage(),
                ),
                500
            );
        }
    }

    /**
     * Method is used to add new Students informations
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Method is used to add new Students informations",
     *  input="v1\StudentsBundle\Form\StudentsType"
     * )
     *
     */
    public function newAction(Request $request)
    {
        $em = $this->getEntityManger();
        $student = new Students();
        $form = $this->createForm('v1\StudentsBundle\Form\StudentsType', $student);

        try {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $student = $this->get('student.cryptor')
                ->encodeDecode_fields($student, ['name', 'emailid', 'dob'], 'encode');

                $em->persist($student);
                $em->flush();

                return $this->renderResponse($student, 'Student details stored', 201);
            }

            return $this->sendError($form->getErrors(true));
        } catch (Exception $ex) {
            return $this->renderResponse(
            null,
            array(
                'message' => $ex->getMessage(),
            ),
            500
        );
        }
    }
    /**
     * Method is used to Finds and displays a Students informations
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Method is used to Finds and displays a Students informations",
     * )
     *
     */
    public function showAction(Students $student)
    {
        if (!$student) {
            return $this->renderResponse(null, 'Students information not available', 404);
        }

        $student = $this->get('student.cryptor')
            ->encodeDecode_fields($student, ['name', 'emailid', 'dob'], 'decode');

        return $this->renderResponse($student);
    }
    /**
     * Method is used to update the existing Students informations
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Method is used to update the existing Students informations",
     *  input="v1\StudentsBundle\Form\StudentsType"
     * )
     */
    public function editAction(Request $request, $id)
    {
        $em = $this->getEntityManger();
        $student = $em
            ->getRepository('DataBundle:Students')
            ->find($id);

        if (!$student) {
            return $this->renderResponse(null, 'Students information not available', 404);
        }
        $form = $this->createForm('v1\StudentsBundle\Form\StudentsType', $student);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($student);
            $em->flush();

            return $this->renderResponse($student, 'Student details updated', 201);
        }

        return $this->sendError($form->getErrors(true));
    }

    /**
     * Method is used to delete a Students entity
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Method is used to delete a Students entity",
     * )
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getEntityManger();
        $student = $em
            ->getRepository('DataBundle:Students')
            ->find($id);

        if (!$student) {
            return $this->renderResponse(null, 'Students information not available', 404);
        } else {
            $em->remove($student);
            $em->flush();

            return $this->renderResponse(null, 'Students deleted successfully', 204);
        }
    }
}
