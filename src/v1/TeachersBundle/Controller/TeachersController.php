<?php

namespace v1\TeachersBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use CoreBundle\Controller\CoreController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use DataBundle\Entity\Teachers;
use DataBundle\Entity\Media;
use DataBundle\Entity\UserDecorator;

/**
 * Teachers controller.
 *
 */
class TeachersController extends CoreController
{
    /**
     * Method used to list all Teachers entities.
     *
     * If limit doesn't given, the default limit 5
     * has taken.
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Method used to list all Teachers entities.",
     * )
     *
     */
    public function indexAction(Request $request)
    {
        try {
            $limit = $request->get('limit', $this->getParameter('limit'));
            $currentPage = $request->get('page', 1);
        
            $searchData['query'] = $request->query->get('query');

            $teacher = $this->get('core.search')
                ->search('DataBundle:Teachers', $searchData, $currentPage, $limit);
            if (!$teacher) {
                return $this->renderResponse(null, 'Teachers information not available', 404);
            } else {
                return $this->renderResponse($teacher);
            }
        } catch (Exception $ex) {
            return $this->renderResponse(
                null,
                array(
                    'message' => $ex->getMessage(),
                ),
                500
            );
        }
    }
  
    /**
     * Method is used to add new Teachers informations
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Method is used to add new Teachers informations",
     *  input="v1\TeachersBundle\Form\TeachersType"
     * )
     *
     */
    public function newAction(Request $request)
    {
        $em = $this->getEntityManger();
        $teacher = new Teachers();
        $form   = $this->createForm('v1\TeachersBundle\Form\TeachersType', $teacher);
        
        try {
            $form->handleRequest($request);
                
            if ($form->isSubmitted() && $form->isValid()) {
                $password = $form["password"]->getData();
                $encrpytPassword = UserDecorator::encrypt_decrypt("encrypt", $password);
                $teacher->setPassword($encrpytPassword);
                $em->persist($teacher);
                $em->flush();
            
                return $this->renderResponse($teacher, 'Teachers details stored', 201);
            }
        
            return $this->sendError($form->getErrors(true));
        } catch (Exception $ex) {
            return $this->renderResponse(
            null,
            array(
                'message' => $ex->getMessage(),
            ),
                500
            );
        }
    }
    /**
    * Method is used to manage Teachers and Students
    *
    * @ApiDoc(
    *  resource=true,
    *  description="Method is used to manage Teachers and Students",
    *  input="v1\TeachersBundle\Form\TeachersStudentsType"
    * )
    * @ParamConverter("teacherId", class="DataBundle:Teachers", options={"id" = "teachers_id"})
    */
    public function manageTeachersStudentsAction(Request $request, $teacherId)
    {
        $em = $this->getEntityManger();
        $teacher = $this->getEntityManger()
                ->getRepository('DataBundle:Teachers')
                ->find($teacherId);
        
        if (!$teacher) {
            return $this->renderResponse(null, 'Teachers information not available', 404);
        }
        
        $form = $this->createForm('v1\TeachersBundle\Form\TeachersStudentsType', $teacher);
        $form->handleRequest($request);
                
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($teacher);
            $em->flush();
            
            return $this->renderResponse(null, 'Teachers and Students mapping successfully updated', 201);
        }
        
        return $this->sendError($form->getErrors(true));
    }
    /**
     * Method is used to Finds and displays a Teachers informations
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Method is used to Finds and displays a Teachers informations",
     * )
     *
     */
    public function showAction(Teachers $teacher)
    {
        if (!$teacher) {
            return $this->renderResponse(null, 'Teachers information not available', 404);
        }
        
        return $this->renderResponse($teacher);
    }
    /**
     * Method is used to Finds and displays a Teachers Media informations
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Method is used to Finds and displays a Teachers Media informations",
     * )
     *
     */
    public function showMediaAction(Request $request, Teachers $teacher)
    {
        if (!$teacher) {
            return $this->renderResponse(null, 'Teachers information not available', 404);
        }
        $limit = $request->get('limit', $this->getParameter('limit'));
        $offset = $request->get('offset');
        
        $em = $this->getEntityManger();
        $teacherid = $teacher->getId();
        $media = $em
                ->getRepository('DataBundle:Media')
                ->findBy(array('teacherid' => $teacherid), array('id'=>'DESC'), $limit, $offset);


        return $this->renderResponse($media);
    }
    /**
     * Method is used to update the existing Teachers informations
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Method is used to update the existing Teachers informations",
     *  input="v1\TeachersBundle\Form\TeachersType"
     * )
     */
    public function editAction(Request $request, $id)
    {
        $em = $this->getEntityManger();
        $teacher = $em
                ->getRepository('DataBundle:Teachers')
                ->find($id);
        
        if (!$teacher) {
            return $this->renderResponse(null, 'Teachers information not available', 404);
        }
        $form = $this->createForm('v1\TeachersBundle\Form\TeachersType', $teacher);
        
        $form->handleRequest($request);
                
        if ($form->isSubmitted() &&  $form->isValid()) {
            $em->persist($teacher);
            $em->flush();
            
            return $this->renderResponse($teacher, 'Teachers details updated', 201);
        }
        
        return $this->sendError($form->getErrors(true));
    }
   
    /**
     * Method is used to delete a Teachers entity
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Method is used to delete a Teachers entity",
     * )
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getEntityManger();
        $teacher = $em
                ->getRepository('DataBundle:Teachers')
                ->find($id);
    
        if (!$teacher) {
            return $this->renderResponse(null, 'Teachers information not available', 404);
        } else {
            $em->remove($teacher);
            $em->flush();
            
            return $this->renderResponse(null, 'Teachers deleted successfully', 204);
        }
    }
}
