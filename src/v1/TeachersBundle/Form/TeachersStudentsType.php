<?php 

namespace v1\TeachersBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\ORM\EntityManager;
use v1\TeachersBundle\Form\EventListener\TeachersStudentsSubscriber;
use v1\TeachersBundle\Form\DataTransformer\TeachersStudentsTransformer;

class TeachersStudentsType extends AbstractType
{
    protected $em;
    
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }    /**
     * @param FormBuilderInterface $builder
     * @param array $options 
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('studentid', CollectionType::class, array(
                'entry_type' => TextType::class,
                'invalid_message' => 'One or more book provided is invalid',
                'allow_add'    => true,
                'allow_delete' => true,
                'by_reference' => false,
                'prototype' => true,
            ));
            
        $builder->addEventSubscriber(new TeachersStudentsSubscriber());
        $builder->get('studentid')->addModelTransformer(new TeachersStudentsTransformer($this->em));
    }
            
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'DataBundle\Entity\Teachers'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return null;
    }
}
