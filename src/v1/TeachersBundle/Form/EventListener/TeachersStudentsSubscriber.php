<?php

namespace v1\TeachersBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class TeachersStudentsSubscriber implements EventSubscriberInterface 
{
    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::PRE_SUBMIT => 'onPreSubmitData',
        );
    }
    
    public function onPreSubmitData(FormEvent $event)
    {
        $modelData = $event->getForm()->getData();
        $reqData = $event->getData();
        
        $studentidCollection = $modelData->getStudentid();
        
        $existingStudentsIds = $studentidCollection->map(function($entity) {
                    return $entity->getId();
                })->toArray();
        
        if (is_array($reqData['studentid'])) {
            $reqData['studentid'] = array_merge($existingStudentsIds, $reqData['studentid']);
        }
        
        $event->setData($reqData);
    }
}
