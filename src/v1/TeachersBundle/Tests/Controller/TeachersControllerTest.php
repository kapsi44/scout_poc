<?php

namespace v1\TeachersBundle\Tests\Controller;

use CoreBundle\Test\ApiTestCase;

class TeachersControllerTest extends ApiTestCase
{
    private function teachersDetails()
    {
        $teachersData = array(
            'name' => 'evFhytZgOGRLEqRLxljrPgobxlVKuhoZEyzegkbhALVoRiWVvfJqmXzZAHvdsCDhcKVsJfsjbOziKIlSZGPXuHrBEBMeZBYZnbVT',
            'emailid' => 'trHWiKTLYTFpDolyshlDaAdfltkjAiLdujOqYcitMGNUnvbQrRUfElrHjBHjFRFfLgmgwKypQxgBswRpFDuvEMgcVRRNtYMEriHJ',
            'password' => 'qWKneZjeIOChbAEcdVpmQaFNWCSUIBdsinYqjHoaBhAnfMguHMzQmCpeETBvwAuFEGZtHJYFnjumHOfmboPRKRwdhhnysCYFOtgO',
            'status' => 'UBoDOIkpkb',
            'createddate'=> '1995-05-05',

        );       
        
        return $teachersData;
    }
    
    public function testIndexAction()
    {
        $url = $this->generateUrl('teachers');

        $crawler = $this->doRequest('GET', $url);
        $this->assertSuccessCode($this->client->getResponse(), "Unexpected HTTP status code for index Teachers GET actions");
    
    }
    
    public function testNewAction()
    {
        $url = $this->generateUrl('teachers_new');
        $fieldWithValues = $this->teachersDetails();
        $crawler = $this->doRequest('POST', $url, $fieldWithValues);
        $this->assertSuccessCode($this->client->getResponse(), "Unexpected HTTP status code for new Teachers POST action");
        
        $teachersResult = $this->getResponseInArray($this->client->getResponse());
        $this->setId($teachersResult['id']);
        
    }
    
    public function testShowAction()
    {
        $url = $this->generateUrl('teachers_show', array(
            'id' => $this->getId()
        ));

        $fieldWithValues = $this->teachersDetails();
        $crawler = $this->doRequest('GET', $url);
        
        $this->assertSuccessCode($this->client->getResponse(), "Unexpected HTTP status code for show Teachers GET action");
        
        $this->assertResponseMatch($this->client->getResponse(), $fieldWithValues);
    }
    
    public function testEditAction()
    {
        $url = $this->generateUrl('teachers_edit', array(
            'id' => $this->getId()
        ));

        $fieldWithValues = $this->teachersDetails();
        $fieldWithValues['name'] = 'TVbnZYBZeMBEBrHuXPGZSlIKizObjsfJsVKchDCsdvHAZzXmqJfvVWiRoVLAhbkgezyEZohuKVlxbogPrjlxLRqELRGOgZtyhFve';
        $fieldWithValues['emailid'] = 'JHirEMYtNRRVcgMEvuDFpRwsBgxQpyKwgmgLfFRFjHBjHrlEfURrQbvnUNGMticYqOjudLiAjktlfdAaDlhsyloDpFTYLTKiWHrt';
        $fieldWithValues['password'] = 'OgtOFYCsynhhdwRKRPobmfOHmujnFYJHtZGEFuAwvBTEepCmQzMHugMfnAhBaoHjqYnisdBIUSCWNFaQmpVdcEAbhCOIejZenKWq';
        $fieldWithValues['status'] = 'bkpkIODoBU';
        $fieldWithValues['createddate'] = '1997-07-07';
        $crawler = $this->doRequest('POST', $url, $fieldWithValues);
        $this->assertSuccessCode($this->client->getResponse(), "Unexpected HTTP status code for update Teachers POST action");
        
        $url = $this->generateUrl('teachers_show', array(
            'id' => $this->getId()
        ));

        
        $crawler = $this->doRequest('GET', $url);
        
        $this->assertSuccessCode($this->client->getResponse(), "Unexpected HTTP status code for Teachers GET action");
        
        $this->assertResponseMatch($this->client->getResponse(), $fieldWithValues);
    }
    
    public function testDeleteAction()
    {
        $url = $this->generateUrl('teachers_delete', array(
            'id' => $this->getId()
        ));

        $crawler = $this->doRequest('DELETE', $url);
        
        $this->assertSuccessCode($this->client->getResponse(), "Unexpected HTTP status code for DELETE");

        $url = $this->generateUrl('teachers_show', array(
            'id' => $this->getId()
        ));

        
        $crawler = $this->doRequest('GET', $url);
        
        $this->assertNotFoundException($this->client->getResponse(), "Unexpected HTTP status code for Teachers GET after delete action");
    }
    
}
