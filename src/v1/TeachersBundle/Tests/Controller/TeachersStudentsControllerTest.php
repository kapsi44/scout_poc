<?php

namespace v1\TeachersBundle\Tests\Controller;

use CoreBundle\Test\ApiTestCase;

class TeachersStudentsControllerTest extends ApiTestCase
{
    private function teachersDetails()
    {
        $teachersData = array(
            'name' => 'evFhytZgOGRLEqRLxljrPgobxlVKuhoZEyzegkbhALVoRiWVvfJqmXzZAHvdsCDhcKVsJfsjbOziKIlSZGPXuHrBEBMeZBYZnbVT',
            'emailid' => 'trHWiKTLYTFpDolyshlDaAdfltkjAiLdujOqYcitMGNUnvbQrRUfElrHjBHjFRFfLgmgwKypQxgBswRpFDuvEMgcVRRNtYMEriHJ',
            'password' => 'qWKneZjeIOChbAEcdVpmQaFNWCSUIBdsinYqjHoaBhAnfMguHMzQmCpeETBvwAuFEGZtHJYFnjumHOfmboPRKRwdhhnysCYFOtgO',
            'status' => 'UBoDOIkpkb',
            'createddate'=> '1995-05-05',

        );       
        
        return $teachersData;
    }
    
    
    
    public function testNewTeachersAction()
    {
        $url = $this->generateUrl('teachers_new');

        $fieldWithValues = $this->teachersDetails();
        $crawler = $this->doRequest('POST', $url, $fieldWithValues);
        $this->assertSuccessCode($this->client->getResponse(), "Unexpected HTTP status code for new Teachers POST action");
        
        $teachersResult = $this->getResponseInArray($this->client->getResponse());
        $this->setId($teachersResult['id']);
        
    }
    
    private static $studentsId;
    
    private function setStudentsId($studentsId)
    {
        self::$studentsId = $studentsId;
        
        return $this;
    }
    
    private function getStudentsId()
    {
        return self::$studentsId;
    }
    
    private function studentsDetails()
    {
        $studentsData = array(
            'name' => 'QgYNLmAMGRJyZBEdDJUFHCHJVDuWuanFlVBOaCzVVyIiGeoRFhGpuedqrjyBHzlRejDoGasEZLSjnFbDhUxFVXUQEFepCspsDpCD',
            'emailid' => 'ttVXTGWxhsQroEOLYlasRLumbAAMBNqrQgNCOjAnPAnFbuLkdCMDlRNaTpfUYwzNUoAhkwUXaREqsenOzAUZLszGjqiMglmPpzCo',
            'dob' => 'pkdLGnpUQHUUvnTTkDITBdbIrkFHbuNWFKEYsnIxmgTLjYZWJbBAxGAtrNSZhoPQeVwlUJxolHKMcYeHTpmtsSATzSUyqKrRSsJW',
            'grade' => 'enCMFeTIms',
            'status' => 'TMWBpbkxWw',
            'createddate'=> '1995-05-05',
        );
        
        return $studentsData;
    }
    
    public function testNewStudentsAction()
    {
        $url = $this->generateUrl('students_new');
        $fieldWithValues = $this->studentsDetails();
        
        $crawler = $this->doRequest('POST', $url, $fieldWithValues);
        
        $this->assertSuccessCode($this->client->getResponse(), "Unexpected HTTP status code for new Students POST action");
        
        $studentsResult = $this->getResponseInArray($this->client->getResponse());
        $this->setStudentsId($studentsResult['id']);
        
    }


    public function testManageTeachersStudentsAction()
    {
        $url = $this->generateUrl('teachers_studentid_manage', array(
            'teachers_id' => $this->getId(),
        ));
        
        $fieldMapping = array(
            'studentid' => array(
                0 => $this->getStudentsId()
            )
        );
        
        $crawler = $this->doRequest('POST', $url, $fieldMapping);
        
        $this->assertSuccessCode($this->client->getResponse(), "Unexpected HTTP status code for manage Teachers POST action");
    }

}