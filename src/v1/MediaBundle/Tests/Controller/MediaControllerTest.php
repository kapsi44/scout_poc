<?php

namespace v1\MediaBundle\Tests\Controller;

use CoreBundle\Test\ApiTestCase;

class MediaControllerTest extends ApiTestCase
{
    private function mediaDetails()
    {
        $mediaData = array(
            'name' => 'RUMlkZSUdiEiypsfPLAlRsgYOHTdGpdQstcimuQitEmenrYbFlHNpmUrZmzjbaLwBFjTCZtwfkqbUWPvdjDglPAdUoVrNRvvUGzy',
            'type' => 'BWKGoCGyJP',
            'urlpath' => 'mZmYubbhNVYSKXvHiRHMyneqZiRVssUDsDkFcexlgfBwAsEuSqFFdEMWqXGiuOUrEYmnqkrpOqavENzeffqroNqfMkEzyhdSPUMZMDlAHgDwbCIcThWbJuPPwqiyUysWCjTFawxATwmZrrQDwTzAVGTNunXcAbIlLPUsIPmBbntCxCSLqiWXIJCvDWYciFxpZEZgcHZeRSXFOTGyvLeUMFjlARtKlhpMDjnCCyGEwXTChbuklBCshzIledLZEXo',
            'status' => 'AvYjbuHCzd',
            'uploadeddate'=> '1995-05-05',
            'teacherid' => 1,

        );       
        
        return $mediaData;
    }
    
    public function testIndexAction()
    {
        $url = $this->generateUrl('media');

        $crawler = $this->doRequest('GET', $url);
        $this->assertSuccessCode($this->client->getResponse(), "Unexpected HTTP status code for index Media GET actions");
    
    }
    
    public function testNewAction()
    {
        $url = $this->generateUrl('media_new');
        $fieldWithValues = $this->mediaDetails();
        $crawler = $this->doRequest('POST', $url, $fieldWithValues);
        $this->assertSuccessCode($this->client->getResponse(), "Unexpected HTTP status code for new Media POST action");
        
        $mediaResult = $this->getResponseInArray($this->client->getResponse());
        $this->setId($mediaResult['id']);
        
    }
    
    public function testShowAction()
    {
        $url = $this->generateUrl('media_show', array(
            'id' => $this->getId()
        ));

        $fieldWithValues = $this->mediaDetails();
        $crawler = $this->doRequest('GET', $url);
        
        $this->assertSuccessCode($this->client->getResponse(), "Unexpected HTTP status code for show Media GET action");
        
        $this->assertResponseMatch($this->client->getResponse(), $fieldWithValues);
    }
    
    public function testEditAction()
    {
        $url = $this->generateUrl('media_edit', array(
            'id' => $this->getId()
        ));

        $fieldWithValues = $this->mediaDetails();
        $fieldWithValues['name'] = 'yzGUvvRNrVoUdAPlgDjdvPWUbqkfwtZCTjFBwLabjzmZrUmpNHlFbYrnemEtiQumictsQdpGdTHOYgsRlALPfspyiEidUSZklMUR';
        $fieldWithValues['type'] = 'PJyGCoGKWB';
        $fieldWithValues['urlpath'] = 'oXEZLdelIzhsCBlkubhCTXwEGyCCnjDMphlKtRAljFMUeLvyGTOFXSReZHcgZEZpxFicYWDvCJIXWiqLSCxCtnbBmPIsUPLlIbAcXnuNTGVAzTwDQrrZmwTAxwaFTjCWsyUyiqwPPuJbWhTcICbwDgHAlDMZMUPSdhyzEkMfqNorqffezNEvaqOprkqnmYErUOuiGXqWMEdFFqSuEsAwBfglxecFkDsDUssVRiZqenyMHRiHvXKSYVNhbbuYmZm';
        $fieldWithValues['status'] = 'dzCHubjYvA';
        $fieldWithValues['uploadeddate'] = '1997-07-07';
        $fieldWithValues['teacherid'] = 1;
        $crawler = $this->doRequest('POST', $url, $fieldWithValues);
        $this->assertSuccessCode($this->client->getResponse(), "Unexpected HTTP status code for update Media POST action");
        
        $url = $this->generateUrl('media_show', array(
            'id' => $this->getId()
        ));

        
        $crawler = $this->doRequest('GET', $url);
        
        $this->assertSuccessCode($this->client->getResponse(), "Unexpected HTTP status code for Media GET action");
        
        $this->assertResponseMatch($this->client->getResponse(), $fieldWithValues);
    }
    
    public function testDeleteAction()
    {
        $url = $this->generateUrl('media_delete', array(
            'id' => $this->getId()
        ));

        $crawler = $this->doRequest('DELETE', $url);
        
        $this->assertSuccessCode($this->client->getResponse(), "Unexpected HTTP status code for DELETE");

        $url = $this->generateUrl('media_show', array(
            'id' => $this->getId()
        ));

        
        $crawler = $this->doRequest('GET', $url);
        
        $this->assertNotFoundException($this->client->getResponse(), "Unexpected HTTP status code for Media GET after delete action");
    }
    
}
