<?php

namespace v1\MediaBundle\Tests\Controller;

use CoreBundle\Test\ApiTestCase;

class MediaStudentsControllerTest extends ApiTestCase
{
    private function mediaDetails()
    {
        $mediaData = array(
            'name' => 'RUMlkZSUdiEiypsfPLAlRsgYOHTdGpdQstcimuQitEmenrYbFlHNpmUrZmzjbaLwBFjTCZtwfkqbUWPvdjDglPAdUoVrNRvvUGzy',
            'type' => 'BWKGoCGyJP',
            'urlpath' => 'mZmYubbhNVYSKXvHiRHMyneqZiRVssUDsDkFcexlgfBwAsEuSqFFdEMWqXGiuOUrEYmnqkrpOqavENzeffqroNqfMkEzyhdSPUMZMDlAHgDwbCIcThWbJuPPwqiyUysWCjTFawxATwmZrrQDwTzAVGTNunXcAbIlLPUsIPmBbntCxCSLqiWXIJCvDWYciFxpZEZgcHZeRSXFOTGyvLeUMFjlARtKlhpMDjnCCyGEwXTChbuklBCshzIledLZEXo',
            'status' => 'AvYjbuHCzd',
            'uploadeddate'=> '1995-05-05',
            'teacherid' => 1,

        );       
        
        return $mediaData;
    }
    
    
    
    public function testNewMediaAction()
    {
        $url = $this->generateUrl('media_new');

        $fieldWithValues = $this->mediaDetails();
        $crawler = $this->doRequest('POST', $url, $fieldWithValues);
        $this->assertSuccessCode($this->client->getResponse(), "Unexpected HTTP status code for new Media POST action");
        
        $mediaResult = $this->getResponseInArray($this->client->getResponse());
        $this->setId($mediaResult['id']);
        
    }
    
    private static $studentsId;
    
    private function setStudentsId($studentsId)
    {
        self::$studentsId = $studentsId;
        
        return $this;
    }
    
    private function getStudentsId()
    {
        return self::$studentsId;
    }
    
    private function studentsDetails()
    {
        $studentsData = array(
            'name' => 'QRgldXPnVdMFHoaogGDJilhzBYFjQDjsuggqMPDprqiWsTvKnZFROVTwICrqRugXGFIYEtZREwtRQeWtmgIbykyVajCAdYzSmFDN',
            'emailid' => 'UfygCbGPUVESdkKRuIZEnxLuZUJEvjfFNjIhTvQJphYByzGlYoYrbFHwmIeggVqNLAntcDVyKuhfCUhkyqrjtiqNXFgWrFQubBjB',
            'dob' => 'nKfPkVZfAPsPpVXaimnIioKBMtzrStJblbHCEarfXorNwPcvtwLhwudNVcLtMjeQAlLFTIvxnGkOkJOKllBvwlVzYinZqCwVdtqU',
            'grade' => 'QeFRfNdXWC',
            'status' => 'cGmynVtWhI',
            'createddate'=> '1995-05-05',
        );
        
        return $studentsData;
    }
    
    public function testNewStudentsAction()
    {
        $url = $this->generateUrl('students_new');
        $fieldWithValues = $this->studentsDetails();
        
        $crawler = $this->doRequest('POST', $url, $fieldWithValues);
        
        $this->assertSuccessCode($this->client->getResponse(), "Unexpected HTTP status code for new Students POST action");
        
        $studentsResult = $this->getResponseInArray($this->client->getResponse());
        $this->setStudentsId($studentsResult['id']);
        
    }


    public function testManageMediaStudentsAction()
    {
        $url = $this->generateUrl('media_studentsid_manage', array(
            'media_id' => $this->getId(),
        ));
        
        $fieldMapping = array(
            'studentsid' => array(
                0 => $this->getStudentsId()
            )
        );
        
        $crawler = $this->doRequest('POST', $url, $fieldMapping);
        
        $this->assertSuccessCode($this->client->getResponse(), "Unexpected HTTP status code for manage Media POST action");
    }

}