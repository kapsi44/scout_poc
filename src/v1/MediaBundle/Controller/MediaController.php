<?php

namespace v1\MediaBundle\Controller;

use CoreBundle\Controller\CoreController;
use DataBundle\Entity\Media;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Media controller.
 *
 */
class MediaController extends CoreController
{
    /**
     * Method used to list all Media entities.
     *
     * If limit doesn't given, the default limit 5
     * has taken.
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Method used to list all Media entities.",
     * )
     *
     */
    public function indexAction(Request $request)
    {
        $limit = $request->get('limit', $this->getParameter('limit'));
        $currentPage = $request->get('page', 1);

        try {
            $searchData['query'] = $request->query->get('query');

            $medium = $this->get('core.search')
                ->search('DataBundle:Media', $searchData, $currentPage, $limit);
            if (!$medium) {
                return $this->renderResponse(null, 'Media information not available', 404);
            } else {
                return $this->renderResponse($medium);
            }
        } catch (Exception $ex) {
            return $this->renderResponse(
                null,
                array(
                    'message' => $ex->getMessage(),
                ),
                500
            );
        }
    }

    /**
     * Method is used to add new Media informations
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Method is used to add new Media informations",
     *  input="v1\MediaBundle\Form\MediaType"
     * )
     *
     */
    public function newAction(Request $request)
    {
        $em = $this->getEntityManger();
        $medium = new Media();
        $form = $this->createForm('v1\MediaBundle\Form\MediaType', $medium);

        try {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                // $file stores the uploaded media file
                $file = $medium->getUrlpath();
                $imgExtn = ['jpg', 'jpeg', 'png', 'gif', 'bmp'];
                $vidExtn = ['mp4', 'webm', 'avi', 'mov', 'flv'];
                $fileExtn = $file->guessExtension();
                $fileName = $this->generateUniqueFileName() . '.' . $fileExtn;

                //gets file original name/type and stores in the name/type field
                $medium->setType($fileExtn);
                $medium->setName($file->getClientOriginalName());
                $medium->setUploadedDate(new \DateTime('now'));
                $medium->setStatus('active');

                // updates the 'media.name' property to store the media file name
                // instead of its contents
                $medium->setUrlpath($fileName);

                // moves the file to the directory where media are stored
                if (in_array($fileExtn, $imgExtn)) {
                    $file->move(
                        $this->getParameter('image_directory'),
                        $fileName
                    );
                } elseif (in_array($fileExtn, $vidExtn)) {
                    $file->move(
                        $this->getParameter('video_directory'),
                        $fileName
                    );
                } else {
                    $file->move(
                        $this->getParameter('media_directory'),
                        $fileName
                    );
                }

                $em->persist($medium);
                $em->flush();

                return $this->renderResponse($medium, 'Media is stored', 201);
            }

            return $this->sendError($form->getErrors(true));
        } catch (Exception $ex) {
            return $this->renderResponse(
                null,
                array(
                    'message' => $ex->getMessage(),
                ),
                500
            );
        }
    }
    /**
     * Method is used to manage Media and Students
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Method is used to manage Media and Students",
     *  input="v1\MediaBundle\Form\MediaStudentsType"
     * )
     * @ParamConverter("mediumId", class="DataBundle:Media", options={"id" = "media_id"})
     */
    public function manageMediaStudentsAction(Request $request, $mediumId)
    {
        $em = $this->getEntityManger();
        $medium = $this->getEntityManger()
            ->getRepository('DataBundle:Media')
            ->find($mediumId);

        if (!$medium) {
            return $this->renderResponse(null, 'Media information not available', 404);
        }

        $form = $this->createForm('v1\MediaBundle\Form\MediaStudentsType', $medium);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($medium);
            $em->flush();

            return $this->renderResponse(null, 'Media and Students mapping successfully updated', 201);
        }

        return $this->sendError($form->getErrors(true));
    }
    /**
     * Method is used to Finds and displays a Media informations
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Method is used to Finds and displays a Media informations",
     * )
     *
     */
    public function showAction(Media $medium)
    {
        if (!$medium) {
            return $this->renderResponse(null, 'Media information not available', 404);
        }

        return $this->renderResponse($medium);
    }
    /**
     * Method is used to update the existing Media informations
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Method is used to update the existing Media informations",
     *  input="v1\MediaBundle\Form\MediaType"
     * )
     */
    public function editAction(Request $request, $id)
    {
        $em = $this->getEntityManger();
        $medium = $em
            ->getRepository('DataBundle:Media')
            ->find($id);

        if (!$medium) {
            return $this->renderResponse(null, 'Media information not available', 404);
        }
        $form = $this->createForm('v1\MediaBundle\Form\MediaType', $medium);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($medium);
            $em->flush();

            return $this->renderResponse($medium, 'Media information updated', 201);
        }

        return $this->sendError($form->getErrors(true));
    }

    /**
     * Method is used to delete a Media entity
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Method is used to delete a Media entity",
     * )
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getEntityManger();
        $medium = $em
            ->getRepository('DataBundle:Media')
            ->find($id);

        if (!$medium) {
            return $this->renderResponse(null, 'Media information not available', 404);
        } else {
            $em->remove($medium);
            $em->flush();

            return $this->renderResponse(null, 'Media deleted successfully', 204);
        }
    }
    /**
     * To get media content
     */
    public function downloadAction($id, $type = '')
    {
        try {
            $em = $this->getEntityManger();
            $file = $em
                ->getRepository('DataBundle:Media')
                ->find($id);

            if (!$file) {
                $array = array(
                    'status' => 0,
                    'message' => 'File does not exist',
                );
                $response = new JsonResponse($array, 200);

                return $response;
            }
            $displayName = $file->getName();
            $fileName = $file->getUrlpath();
            $imgExtn = ['jpg', 'jpeg', 'png', 'gif', 'bmp'];
            $vidExtn = ['mp4', 'webm', 'avi', 'mov', 'flv'];
            $fileExtn = $file->getType();
            $file_with_path = '';

            if (in_array($fileExtn, $imgExtn)) {
                $file_with_path = $this->container->getParameter('image_directory') . "/" . $fileName;
                $response = new BinaryFileResponse($file_with_path);
                $response->headers->set('Content-Type', 'image/jpg');
            } elseif (in_array($fileExtn, $vidExtn)) {
                $file_with_path = $this->container->getParameter('video_directory') . "/" . $fileName;
                $response = new BinaryFileResponse($file_with_path);
                $response->headers->set('Content-Type', 'video/mp4');
            } else {
                $file_with_path = $this->container->getParameter('media_directory') . "/" . $fileName;
                $response = new BinaryFileResponse($file_with_path);
                $response->headers->set('Content-Type', 'text/plain');
            }

            $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $displayName);
            return $response;
            // }
        } catch (Exception $e) {
            $array = array(
                'status' => 0,
                'message' => 'Download error',
            );
            $response = new JsonResponse($array, 400);
            return $response;
        }
    }

    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }
}
