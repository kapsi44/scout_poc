<?php 

namespace v1\MediaBundle\Form\DataTransformer;

use DataBundle\Entity\Students;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException; 

class MediaStudentsTransformer implements DataTransformerInterface
{
    private $manager;

    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }
    
    /**
     * Method to convert model data to form.
     *
     * @param  Model|null $entity
     * @return string|array
     */
    public function transform($entity)
    {
        return $entity;
    }
    
    /**
     * Method to convert form data to model.
     *
     * @param  array $entities
     * @return Students|null
     * @throws TransformationFailedException if object (issue) is not found.
     */
    public function reverseTransform($entities)
    {
        if (!$entities) {
            return;
        }

        $data = array();
        foreach ($entities as $key => $entity) {
            $model = $this->manager->getRepository('DataBundle:Students')->find($entity);
            if (null === $model) {
                throw new TransformationFailedException('Failed transformation');
            }
            $data[$key] = $model;
        }

        return $data;
    }
}