<?php

namespace v1\MediaBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class MediaStudentsSubscriber implements EventSubscriberInterface 
{
    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::PRE_SUBMIT => 'onPreSubmitData',
        );
    }
    
    public function onPreSubmitData(FormEvent $event)
    {
        $modelData = $event->getForm()->getData();
        $reqData = $event->getData();
        
        $studentsidCollection = $modelData->getStudentsid();
        
        $existingStudentsIds = $studentsidCollection->map(function($entity) {
                    return $entity->getId();
                })->toArray();
        
        if (is_array($reqData['studentsid'])) {
            $reqData['studentsid'] = array_merge($existingStudentsIds, $reqData['studentsid']);
        }
        
        $event->setData($reqData);
    }
}
