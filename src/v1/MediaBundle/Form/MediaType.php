<?php

namespace v1\MediaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class MediaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options 
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                // ->add('name')
                // ->add('type')
                ->add('urlpath', FileType::class, array('label' => 'media'))
                // ->add('status')
                // ->add('uploadeddate', DateType::class, array(
                    // 'widget' => 'single_text',
                    // ))
                ->add('teacherid', EntityType::class, array(
                'class' => 'DataBundle\Entity\Teachers',
                'choice_label' => 'name',
                'placeholder' => 'Please choose',
                'empty_data' => null,
                'required' => false
 

            )) 
            ->add('studentsid', EntityType::class, array(

                'class' => 'DataBundle\Entity\Students',
                'choice_label' => 'name',
                'expanded' => true,
                'multiple' => true

            )) 
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'DataBundle\Entity\Media',
            'csrf_protection' => false
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return null;
    }

}
