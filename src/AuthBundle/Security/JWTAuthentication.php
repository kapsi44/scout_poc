<?php 
namespace AuthBundle\Security;

use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Doctrine\ORM\EntityManager;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\DefaultEncoder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Lexik\Bundle\JWTAuthenticationBundle\TokenExtractor\AuthorizationHeaderTokenExtractor;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use DataBundle\Entity\UserDecorator;

class JWTAuthentication extends AbstractGuardAuthenticator
{
    private $em;
    private $jwtEncoder;
    private $loginRepo;
    private $loginUser;

    public function __construct(EntityManager $em, DefaultEncoder $jwtEncoder, $loginRepo, $loginUser)
    {
        $this->em = $em;
        $this->jwtEncoder = $jwtEncoder;
        $this->loginRepo = $loginRepo;
        $this->loginUser = $loginUser;
    }
    
    public function start(Request $request, AuthenticationException $authException = null)
    {
        return new JsonResponse('Auth header required', 401);
    }
    
    public function getCredentials(Request $request)
    {
        if (!$request->headers->has('Authorization')) {
            return;
        }
        
        $extractor = new AuthorizationHeaderTokenExtractor(
            'Bearer',
            'Authorization'
        );
        
        $token = $extractor->extract($request);
        
        if (!$token) {
            return;
        } else {
            return $token;
        }
        
    }
    
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $data = $this->jwtEncoder->decode($credentials);
        
        if (!$data) {
            return;
        }

        $username = $data['username'];
        
        $login = $this->em->getRepository($this->loginRepo)
                    ->findOneBy([
                        $this->loginUser => $username
                    ]);
        
        $user = new UserDecorator($login);
        
        if (!$user) {
            return;
        } else {
            return $user;
        }
    }
    
    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }
    
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        
        return new JsonResponse([
            'message' => $exception->getMessage()
        ], 401);
    }
    
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return;
    }
    
    public function supportsRememberMe()
    {
        return;
    }
}

