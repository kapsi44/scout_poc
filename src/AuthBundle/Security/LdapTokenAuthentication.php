<?php

namespace AuthBundle\Security;

use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\DefaultEncoder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Lexik\Bundle\JWTAuthenticationBundle\TokenExtractor\AuthorizationHeaderTokenExtractor;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\User;

class LdapTokenAuthentication extends AbstractGuardAuthenticator
{

    private $ldap;
    private $encoder;
    private $baseDn;
    private $uidKey;
    private $defaultSearch;
    private $defaultRole;

    public function __construct($ldap, DefaultEncoder $encoder, $baseDn = null, array $defaultRoles = array(), $uidKey = 'sAMAccountName', $filter = '({uid_key}={username})')
    {
        $this->ldap = $ldap;
        $this->encoder = $encoder;
        $this->baseDn = $baseDn;
        $this->uidKey = $uidKey;
        $this->defaultRole = $defaultRoles;
        $this->defaultSearch = str_replace('{uid_key}', $uidKey, $filter);
    }
    
    public function start(Request $request, AuthenticationException $authException = null)
    {
        return new JsonResponse('Auth header required', 401);
    }

    public function getCredentials(Request $request)
    {
        if (!$request->headers->has('Authorization')) {
            return;
        }
        
        $extractor = new AuthorizationHeaderTokenExtractor(
            'Bearer',
            'Authorization'
        );
        
        $token = $extractor->extract($request);
        
        if (!$token) {
            return;
        } else {
            return $token;
        }
    }
    
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $data = $this->encoder->decode($credentials);
        
        if (!$data) {
            return;
        }

        $username = $data['username'];
        
        try {
            $searchUsername = $this->getSearchUsername($username);
            $defaultSearch = str_replace('{uid_key}', $this->uidKey, $this->defaultSearch);
            $query = str_replace('{username}', $searchUsername, $defaultSearch);
            $this->ldap->query($this->baseDn, $query);
        } catch (Exception $ex) {
            throw new UsernameNotFoundException(sprintf('User "%s" not found.', $username), 0, $ex);
        }
        
        $user = new User($username, null, $this->defaultRole);

        if (!$user) {
            return;
        } else {
            return $user;
        }
    }
    
    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }
    
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        
        return new JsonResponse([
            'message' => $exception->getMessage()
        ], 401);
    }
    
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return;
    }
    
    public function supportsRememberMe()
    {
        return;
    }
    
    private function getSearchUsername($username)
    {
        /* We can also check with / delimiter. ex: Username may be have domain/username.
         * Now check with @ delimiter only.
         */
        $searchUsername = strstr($username, '@', true);
        
        return ucwords(join(' ', explode('.', $searchUsername)));
    }
}

