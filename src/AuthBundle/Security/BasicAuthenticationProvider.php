<?php
namespace AuthBundle\Security;

use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use DataBundle\Entity\UserDecorator;
use Doctrine\ORM\EntityManager;

class BasicAuthenticationProvider implements UserProviderInterface
{
    private $em;
    private $loginRepo;
    private $loginUsername;
    
    public function __construct(EntityManager $em, $loginRepo, $loginUsername)
    {
        $this->em = $em;
        $this->loginRepo = $loginRepo;
        $this->loginUsername = $loginUsername;
    }
    
    public function loadUserByUsername($username)
    {
        $login = $this->em
                ->getRepository($this->loginRepo)
                ->findOneBy(array(
                    $this->loginUsername => $username,
                ));
        
        if (!$login) {
            throw new UsernameNotFoundException(sprintf('User "%s" not found.', $username));
        }
        
        $user = new UserDecorator($login);
        
        return $user;
    }
    
    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof UserDecorator) {
            throw new UnsupportedUserException(
                sprintf('Instances of "%s" are not supported.', get_class($user))
            );
        }
        
        return $this->loadUserByUsername($user->getUsername());
    }
    
    public function supportsClass($class)
    {
        return UserDecorator::class === $class;
    }
}

