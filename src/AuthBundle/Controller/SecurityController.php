<?php

namespace AuthBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use CoreBundle\Controller\CoreController;
use DataBundle\Entity\UserDecorator;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Ldap\LdapInterface;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Exception;

class SecurityController extends CoreController
{
    private $loginPK = null;
    
    /**
     * Method is used to validate login details and return JWT token
     * 
     * @param Request $request
     * @return JsonResponse
     * @throws type
     * 
     * @ApiDoc(
     *  resource=true,
     *  description="Method is used to validate login details and return JWT token",
     *  parameters={
     *      {"name"="username", "dataType"="string", "required"=true},
     *      {"name"="password", "dataType"="password", "required"=true}
     *  }
     * )
     */
    public function loginAction(Request $request)
    {
        $username = $request->get('username');
        $password = UserDecorator::encrypt_decrypt("encrypt", $request->get('password'));

        try {
            $user = $this->loadUser($username, $password);

            $token = $this->get('lexik_jwt_authentication.encoder')
                ->encode(['username' => $user->getUserName()]);
                
            return $this->renderResponse(
                    null,
                    array(
                        'user_name' => $user->getUserName(),
                        'user_id' => $user->getUserId(),
                        'token' => $token
                    ));
        } catch (Exception $ex) {

            return $this->renderResponse(
                    null,
                    array(
                        'message' => $ex->getMessage(),
                    ), 401);
        }
    }
    
    /**
     * Method is used to validate username and if valid user send email
     * with token for reset password
     * 
     * @param Request $request
     * @return type
     * 
     * @ApiDoc(
     *  resource=true,
     *  description="Method is used to validate username and if valid user send email
     *  with token for reset password",
     *  parameters={
     *      {"name"="username", "dataType"="string", "required"=true}
     *  }
     * )
     */
    public function forgotAction(Request $request)
    {
        $username = $request->get('username');
        
        $login = $this->getDoctrine()->getRepository($this->getParameter('login.repository'))
                ->findOneBy([
                    $this->getParameter('login.lookup_user') => $username,
                   ]);
        
        if (!$login) {
            
            return $this->renderResponse(null, 'Please given valid Username.' , 404);
        } 
        
        $expireTime = $this->getParameter('expire_time');
        $expireDate = date('Y-m-d-h:i:00', strtotime($expireTime));
        
        if ($this->getParameter('login.primary_key') === null && $this->getLoginPk() === null) {
            $this->loadLoginPK();
        }
        
        $loginPK = $this->getParameter('login.primary_key')
                    ? $this->getParameter('login.primary_key')
                    : $this->getLoginPK();
        
        $primaryKey = 'get' . ucfirst($loginPK);
        
        $uniqueId = $login->{$primaryKey}() . '~' . $expireDate;
        
        $mailer = $this->get('core.simple.mailer');

        $key = $this->getParameter('secret');
        $cryptor = $this->get('core.simple.cryptor');

        $token = $cryptor->encrypt($uniqueId, $key);
        
        $resetUrl = $this->generateUrl('api_password_reset');

        $resetLink = $request->server->get('HTTP_HOST') . $resetUrl . '?token=' . $token;

        $body = $this->renderView('CoreBundle:email:resetPassword.txt.twig', array(
            'name' => strstr($username, '@', true),
            'reset_link' => $resetLink,
            'expire_time' => $expireTime
        ));

        $mailer->send('Password Reset Details', $body, $username);
        
        return $this->renderResponse(null, 'We have send password reset details to your registered email.');
    }
    
    /**
     * Method is used to reset the password.
     * 
     * @param Request $request
     * @return type
     * 
     * @ApiDoc(
     *  resource=true,
     *  description="Method is used to reset the password.",
     *  parameters={
     *      {"name"="password", "dataType"="password", "required"=true}
     *  }
     * )
     */
    public function resetAction(Request $request)
    {
        $em = $this->getEntityManger();
        
        $token = $request->query->get('token');
        $password = $request->get('password');
        
        $key = $this->getParameter('secret');
        $cryptor = $this->get('core.simple.cryptor');
        $uniqueId = $cryptor->decrypt($token, $key);
        
        $uniqueId = explode('~', $uniqueId);
        $userId = (int) $uniqueId[0];
        
        if ($uniqueId[1] < date('Y-m-d-h:i:00')) {
            
            return $this->renderResponse(null, 'Your token has expired please regenerate it :(', 410);
        }
        
        $login = $this->getDoctrine()
                ->getRepository($this->getParameter('login.repository'))
                ->find($userId);

        if ($login && !empty($password)) {
            $passwordField = 'set' . ucfirst($this->getParameter('login.lookup_password'));
            $login->{$passwordField}($password);

            $em->persist($login);
            $em->flush();

            return $this->renderResponse(null, 'Your password updated successfully :)');
        } 
        
        return $this->renderResponse(null, 'Something went wrong. :(');
    }
    
    private function loadLoginPK()
    {
        $entityClass = explode('\\', $this->getParameter('login.repository'));
        $entityClass = array_pop($entityClass);
        
        $file = $this->getParameter('login.yaml_file_path') . $entityClass . '.orm.yml';
        $ymlFile = Yaml::parse(file_get_contents($file));
        
        $primayKey = key($ymlFile[$this->getParameter('login.repository')]['id']);
        
        $this->setLoginPK($primayKey);
    }
    
    private function setLoginPK($primayKey)
    {
        $this->loginPK = $primayKey;
        
        return $this->loginPK;
    }

    private function getLoginPK()
    {
        return $this->loginPK;
    }

    /**
     * Load User based on given parameters, If $ldap true it has check User against
     * LDAP server else it has check User by repository.
     *
     * @param   string                    $username
     * @param   string                    $password
     *
     * @return  User|UserDecorator
     * @throws  UsernameNotFoundException
     * @throws  type
     */
    private function loadUser($username, $password)
    {
        if ($this->getParameter('is_ldap_auth')) {
            try {
                $ldapConnection = $this->get('ldap_authentication');
                $username = $username . '@' . $this->getParameter('domain');

                $ldapConnection->bind($username, $password);
            } catch (Exception $ex) {
                throw new UsernameNotFoundException(sprintf('User "%s" not found.', $username), 0, $ex);
            }

            return new User($username, $password, $this->getParameter('deafault_role'));
        }

        $login = $this->getDoctrine()->getRepository($this->getParameter('login.repository'))
                ->findOneBy([
                    $this->getParameter('login.lookup_user') => $username,
                   ]);

        if (!$login) {
            throw new Exception('Please enter valid credential. Username and Password mismatch');
        }

        $user = new UserDecorator($login);

        if(!$this->get('security.password_encoder')->isPasswordValid($user, $password)) {
            throw new Exception('Please enter valid credential. Given Password is invalid');
        }

        return $user;
    }
}
