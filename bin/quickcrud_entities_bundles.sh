#!/bin/bash

php bin/console cache:clear --env=dev

php bin/console doctrine:mapping:import DataBundle yml

php bin/console cache:clear --env=dev

php bin/console doctrine:generate:entities DataBundle --no-backup

php bin/console cache:clear --env=dev

if [ $# -eq 0 ]
	then
    echo "\n No bundle name given \n "
    exit 0
fi

for bundleName in "$@"
do
    php bin/console generate:bundle --namespace='v1/'$bundleName'Bundle' --bundle-name=$bundleName'Bundle' --format=yml --no-interaction
    rm -r src/v1/$bundleName'Bundle'/Controller/DefaultController.php src/v1/$bundleName'Bundle'/Resources/config/routing.yml tests/$bundleName'Bundle'/
    php bin/console ngpropel:accelerate --entity=DataBundle:$bundleName --format=yml -o --no-interaction
done
